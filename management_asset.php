<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Apollo Asset Managment - Apollo Asset</title>
    <link rel="stylesheet" href="css/companies.css" media="screen">

    <?php include 'views/head_links.php'; ?>

</head>

<body>

    <!--==========================
    Header
    ============================-->
    <?php include 'views/nav.php'; ?>
    <!-- #header -->

    <!--==========================
    Intro Section
    ============================-->
    <div class="banner-asset">

    </div>
    <div class="cover-line">
        <img src="images/line.png" alt="">
    </div>


    <!-- #intro -->

    <!-- ===================================== CHILD MENU BAR ===================================== -->

    <div class="company-nav">
        <ul class="nav justify-content-center">
            <li class="nav-item">
                <a class="nav-link" href="companies.php">COMPANIES</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="leadership.php">LEADERSHIP</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="management_group.php">MANAGEMENT</a>
            </li>
            <!-- <li class="nav-item">
                <a class="nav-link" href="#">DOWNLOADS</a>
            </li> -->
        </ul>
    </div>

    <!-- SUB NAV -->

    <div class="sub-nav">
        <ul class="nav justify-content-center">
            <li class="nav-item">
                <a class="nav-link" href="management_group.php">GROUP HEADS </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="management.php">APA INSURANCE </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="management_life.php">APA LIFE </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="management_asset.php">APOLLO ASSET MANAGEMENT</a>
            </li>

        </ul>
    </div>

    <!-- ===================================== ABOUT US ===================================== -->


    <div class="container-fluid12">
        <div class="container apollo">
            <h1 class="wow fadeInUp" data-wow-delay="0.1s">APOLLO ASSET MANAGEMENT </h1>
            <div class="under-line img1">
                <img src="images/line.png" alt="">
            </div>

            <p class="container content-offer wow fadeInUp" data-wow-delay="0.1s">
                AAMC is licensed by the Capital Markets Authority (CMA) and the Retirement
                Benefits Authority (RBA) to conduct Fund Management and Investment Advisory Services.
            </p>
        </div>

        <!-- APA INSURANCE -->
        <div class="company-box apollo">
            <div class="container">

                <div class="row lead-img">

                    <div class="col-md-3 leader-img wow fadeInUp" data-wow-delay="0.1s">
                        <img src="img/apa4/maina.jpg" class="img" alt="">
                        <h5 class="title-name">
                            Maina Wacieni<br>
                            <small>General Manager</small>
                        </h5>

                        <p>
                            Maina has over 10 years of experience in the asset management industry spanning ...
                        </p>
                        <button class="btn btn-primary" data-target="#modal-full" uk-toggle>Read more</button>
                    </div>

                    <div class="col-md-3 leader-img wow fadeInUp" data-wow-delay="0.1s">
                        <img src="img/apa4/kosgei.jpg" class="img" alt="">
                        <h5 class="title-name">
                            Amos Kosgey<br>
                            <small>Portfolio Manager</small>
                        </h5>

                        <p>
                            Amos Kosgey is the Portfolio Manager at Apollo Asset Management Company...
                        </p>
                        <button class="btn btn-primary" data-target="#modal-full1" uk-toggle>Read more</button>
                    </div>

                    <div class="col-md-3 leader-img wow fadeInUp" data-wow-delay="0.1s">
                        <img src="img/passport2/jp.jpg" class="img" alt="">
                        <h5 class="title-name">
                            Johnpaul Barasa<br>
                            <small>Senior Accountant </small>
                        </h5>

                        <p>
                            Johnpaul Barasa is the Senior Accountant at Apollo Asset Management Company...
                        </p>
                        <button class="btn btn-primary" data-target="#modal-full3" uk-toggle>Read more</button>
                    </div>

                    <div class="col-md-3 leader-img wow fadeInUp" data-wow-delay="0.1s">
                        <img src="img/apa4/solo.jpg" class="img" alt="">
                        <h5 class="title-name">
                            Solomon Maonga<br>
                            <small>Investment Executive</small>
                        </h5>

                        <p>
                            Solomon Maonga is the Investment Executive at Apollo Asset Management...
                        </p>
                        <button class="btn btn-primary" data-target="#modal-full2" uk-toggle>Read more</button>
                    </div>


                </div>

                <!-- <div class="row lead-img ">

                    <div class="col-md-3 leader-img wow fadeInUp" data-wow-delay="0.1s">
                        <img src="img/apa4/eva.jpg" class="img" alt="">
                        <h5 class="title-name">
                            Eva Labatt<br>
                            <small>Unit Trust Officer</small>
                        </h5>

                        <p>
                            Eva Labatt is the Unit Trust Officer at Apollo Asset Management Company...
                        </p>
                        <button class="btn btn-primary" data-target="#modal-full4" uk-toggle>Read more</button>
                    </div>

                    <div class="offset-md-9">

                    </div>


                </div> -->

            </div>
        </div>


    </div>





    <!-- =====================================FOOTER===================================== -->
    <?php include 'views/footer.php'; ?>
    <!-- #footer -->

    <!-- =====================================  Fred Mburu ===================================== -->

    <div id="modal-full" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
            <div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle" uk-grid>
                <div class="uk-background-cover" style="background-image: url('img/managment/maina.jpg');" uk-height-viewport></div>
                <div class="uk-padding-large">
                    <h1>
                        Maina Wacieni 
                    </h1>
                    <h2 class="small-head">
                        General Manager
                    </h2>
                    <p class="text-justify">
                    Maina has over 10 years of experience in the asset management industry spanning both 
                    the sell and buy-side. He has been actively involved in portfolio construction and 
                    risk management, equity & fixed income research and analysis across different market 
                    cycles and has subsequently constructively interacted with a smorgasbord of different 
                    types of clientele including high net worth investors, pension funds and standalone 
                    (segregated) institutional investment mandates. He holds a Bachelor of Arts Degree in 
                    Economics from Vassar College in New York and is also a CFA® Charter Holder

                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- ===================================== Amos Kosgei ===================================== -->

    <div id="modal-full1" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
            <div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle" uk-grid>
                <div class="uk-background-cover" style="background-image: url('img/managment/kosgei.jpg');" uk-height-viewport></div>
                <div class="uk-padding-large">
                    <h1>
                        Amos Kosgey
                    </h1>
                    <h2 class="small-head">
                        Portfolio Manager
                    </h2>
                    <p class="text-justify">
                        Amos Kosgey is the Portfolio Manager at Apollo Asset Management Company. He has 12 years experience in the financial
                        services industry. Amos has a Master of Science degree in Economics from Eastern College in Philadelphia, USA and
                        is also a graduate of Daystar University. He also has had training in Fundamentals of Investing, Anti-Money Laundering,
                        and Bond Trading.egies, and create assets and realise investment opportunities
                        existing in-country and emerging markets.

                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- ===================================== Solomon Maoga ===================================== -->

    <div id="modal-full2" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
            <div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle" uk-grid>
                <div class="uk-background-cover" style="background-image: url('img/managment/solo.jpg');" uk-height-viewport></div>
                <div class="uk-padding-large">
                    <h1>
                        Solomon Maonga
                    </h1>
                    <h2 class="small-head">
                        Investment Executive
                    </h2>
                    <p class="text-justify">
                        Solomon Maonga is the Investment Executive at Apollo Asset Management Company.
                        He has 4 years’ experience in the financial services industry.  Solomon has a Bachelor
                        of Business Science degree (BBS) in Financial Economics from Strathmore University and
                        is currently pursuing a Master of Science (MSc) in Development Finance at the Strathmore
                        Business School. He is also an Associate member of the Chartered Institute for Securities
                        and Investment (CISI, UK) and is enrolled as a student for the Chartered Institute of
                        Financial Analysts (CIFA)programme.

                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- ===================================== John Paul ===================================== -->

    <div id="modal-full3" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
            <div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle" uk-grid>
                <div class="uk-background-cover" style="background-image: url('img/passport2/jp.jpg');" uk-height-viewport></div>
                <div class="uk-padding-large">
                    <h1>
                        Johnpaul Barasa
                    </h1>
                    <h2 class="small-head">
                        Senior Accountant
                    </h2>
                    <p class="text-justify">
                        Johnpaul Barasa is the Senior Accountant at Apollo Asset Management Company, Certified Public Accountant of Kenya,
                        graduate of Moi University Bachelors of Business Management(Accounting) with more than 9 years of professional
                        experience in auditing and management accounting, and currently pursuing his final exams in Certified Investment
                        Financial Analysis (CIFA). He also has enrolled as a student under Chartered Institute for Securities Investment
                        (CISI) where he has had training in Fundamentals of Investing; fixed income instruments trading.

                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- ===================================== Everlyne Lagat ===================================== -->

    <div id="modal-full4" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
            <div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle" uk-grid>
                <div class="uk-background-cover" style="background-image: url('img/managment/eva.jpg');" uk-height-viewport></div>
                <div class="uk-padding-large">
                    <h1>
                        Eva Labatt
                    </h1>
                    <h2 class="small-head">
                        Unit Trust Officer
                    </h2>
                    <p class="text-justify">
                        Eva Labatt is the Unit Trust Officer at Apollo Asset Management Company.  She has 7 years’ experience in the
                        financial services industry.  Eva has a Bachelor of Commerce, Finance option from Catholic University of
                        Eastern Africa. She is also an Associate member of the Chartered Institute for Securities and Investment (CISI, UK).

                    </p>
                </div>
            </div>
        </div>
    </div>

    <?php
    require_once 'inc/scripts.php';
    ?>

</body>

</html>