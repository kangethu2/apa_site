<?php

require_once('config/db.php');
require_once('inc/functions.php');
require_once('inc/sessions.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>HR RECRUITMENT PORTAL</title>

  <?php require_once('inc/head_links.php'); ?>

</head>

<body class="bg-gradient-primary">

  <div class="container">
    <br><br><br>

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->

            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-2">Send Reset Link</h1>
                    <?php
                    if (!empty($errors)) {
                      echo display_errors($errors);
                    }
                    echo errorMessage();
                    echo successMessage();
                    ?>
                    <p class="mb-4">Enter your email address below and we'll send you a link to reset your password!</p>
                  </div>
                  <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST" id="form" class="user">
                    <div class="form-group">
                      <input name="email" type="email" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address..." required>
                    </div>
                    <button href="" id="reset_button" class="btn btn-primary btn-user btn-block">
                      Reset Password
                    </button>
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="login.php">Already have an account? Login!</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <?php require_once('inc/js.php'); ?>
  <script>
    $(document).ready(function() {
      $('#form').parsley();
      $('#form').on('submit', function(e) {
        e.preventDefault();
        var dataString = $('#form').serialize();
        $.ajax({
          type: "POST",
          url: "ajax/processor.php?request=reset_link",
          data: dataString,
          dataType: "text",
          success: function(response) {
            if (response == 'success') {
              $('#form')[0].reset();
              swal.fire({
                title: 'Password reset link sent successfully. Check your email',
                type: response,
                allowOutsideClick: true,
                showConfirmButton: true
              });
            } else {
              swal.fire({
                title: response,
                type: 'error',
                allowOutsideClick: true,
                showConfirmButton: true
              });
            }
          }
        });
      });
    });
  </script>
</body>

</html>