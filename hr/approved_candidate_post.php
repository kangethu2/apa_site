<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HR RECRUITMENT PORTAL</title>

    <?php include 'views/head_links.php'; ?>


</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php
    $page = basename($_SERVER['PHP_SELF']);
    include 'views/sidebar.php';
    ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include 'views/nav.php'; ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container live text-center">
                    <br>
                    <h2>APPROVED CANDIDATE</h2>
                    <div class="row">
                        <div class="col-6">
                            <div class="card box-ap mb-4 py-1 border-bottom-primary1">
                                <div class="card-body illustrat-icon text-center">
                                    <h5>BUSINESS INTELLIGENCE OFFICER</h5>
                                    <hr>
                                    <br>
                                    <ul class="text-left">
                                        <li><b>DEPARTMENT</b> : ICT</li>
                                        <li><b>SUPERVISOR</b> : Group Chief Information Officer</li>
                                        <li><b>LOCATION</b> : Nairobi,Kenya</li>
                                        <li><b>EMPLOYMENT TYPE</b> : Full Time</li>
                                    </ul>

                                    <div class="row btn-sub">
                                        <div class="col-4">
                                            <a href="approved_candidate_table.html" class="btn btn-primary">
                                                View candidate
                                            </a>
                                        </div>

                                        <div class="col-4">
                                            <a href="#" class="btn btn-danger">
                                                Delete post
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="card box-ap mb-4 py-1 border-bottom-primary1">
                                <div class="card-body illustrat-icon text-center">
                                    <h5>BUSINESS DEVELOPER</h5>
                                    <hr>
                                    <br>
                                    <ul class="text-left">
                                        <li><b>DEPARTMENT</b> : ICT</li>
                                        <li><b>SUPERVISOR</b> : Group Chief Information Officer</li>
                                        <li><b>LOCATION</b> : Nairobi,Kenya</li>
                                        <li><b>EMPLOYMENT TYPE</b> : Full Time</li>
                                    </ul>

                                    <div class="row btn-sub">
                                        <div class="col-4">
                                            <a href="approved_candidate_table.html" class="btn btn-primary">
                                                View candidate
                                            </a>
                                        </div>



                                        <div class="col-4">
                                            <a href="#" class="btn btn-danger">
                                                Delete post
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->


            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php include 'views/footer.php'; ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>

</body>

</html>