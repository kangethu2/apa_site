<?php
require_once('../config/db.php');
require_once('../inc/functions.php');
require_once('../inc/sessions.php');
switch ($_GET['request']) {
    case 'create_post':
        sleep(1);
        //    validate
        $job_id = randomstring(10);
        $created_at = date('Y-m-d H:i:s');
        $admin = $_SESSION['id'];
        $title = sanitize($_POST['title']);
        $department = sanitize($_POST['department']);
        $reports_to = ((isset($_POST['reports_to'])) ? sanitize($_POST['reports_to']) : '');
        $employment_type = sanitize($_POST['employment_type']);
        $job_short_description = sanitize($_POST['job_short_description']);
        $location = sanitize($_POST['location']);
        $email = sanitize($_POST['email']);
        $company = sanitize($_POST['company']);
        $deadline = sanitize($_POST['deadline']);
        $salary = ((isset($_POST['salary'])) ? filter_var((sanitize($_POST['salary'])), FILTER_SANITIZE_NUMBER_INT) : 0);
        $key_primary_responsibilities = sanitize($_POST['key_primary_responsibilities']);
        $academic_qualifications = sanitize($_POST['academic_qualifications']);
        $professional_qualification = sanitize($_POST['professional_qualification']);
        $job_skills_and_requirements = sanitize($_POST['job_skills_and_requirements']);
        $experience = sanitize($_POST['experience']);
        // insert into db
        $insertquery = "INSERT INTO apa_job_posts ( job_id, job_title,department, reports_to,job_short_description,`location`,`email`,company, employment_type, salary, key_primary_responsibilities, academic_qualifications, professional_qualifications, job_skills_and_requirements, experience, deadline, added_by)
        VALUES('$job_id', '$title', '$department','$reports_to','$job_short_description','$location','$email', '$company','$employment_type', '$salary', '$key_primary_responsibilities', '$academic_qualifications', '$professional_qualification','$job_skills_and_requirements', '$experience', '$deadline', '$admin')";
        $insert = $conn->query($insertquery);
        if ($insert) {
            echo 'success';
        } else {
            echo 'error ' . mysqli_error($conn);
        }
        $conn = null;
        break;

        // update
    case ('update'):
        sleep(1);
        $update_id = sanitize($_GET['id']);
        $job_id = sanitize($_GET['id']);
        $updated_at = date('Y-m-d H:i:s');
        $admin = sanitize($_SESSION['id']);
        $title = sanitize($_POST['title']);
        $department = sanitize($_POST['department']);
        $reports_to = ((isset($_POST['reports_to'])) ? sanitize($_POST['reports_to']) : null);
        $employment_type = sanitize($_POST['employment_type']);
        $job_short_description = sanitize($_POST['job_short_description']);
        $location = sanitize($_POST['location']);
        $email = sanitize($_POST['email']);
        $company = sanitize($_POST['company']);
        $deadline = sanitize($_POST['deadline']);
        $salary = ((isset($_POST['salary'])) ? filter_var((sanitize($_POST['salary'])), FILTER_SANITIZE_NUMBER_INT) : null);
        $key_primary_responsibilities = sanitize($_POST['key_primary_responsibilities']);
        $academic_qualifications = sanitize($_POST['academic_qualifications']);
        $professional_qualification = sanitize($_POST['professional_qualification']);
        $job_skills_and_requirements = sanitize($_POST['job_skills_and_requirements']);
        $experience = sanitize($_POST['experience']);

        $edited_by = sanitize($_SESSION['id']);

        $updateQuery = 'UPDATE apa_job_posts SET job_title= ?, department=?, reports_to=?, `location`=?,`email`=?, company=?, employment_type=?, salary=?, job_short_description=?, key_primary_responsibilities=?, academic_qualifications=?, professional_qualifications=?, job_skills_and_requirements=?, deadline=? , edited_by=? WHERE job_id = ? ';
        $stmt = $conn->prepare($updateQuery);
        $stmt->execute([
            $title, $department, $reports_to, $location, $email, $company, $employment_type, $salary, $job_short_description, $key_primary_responsibilities, $academic_qualifications, $professional_qualification, $job_skills_and_requirements, $deadline, $edited_by, $job_id,
        ]);
        if ($stmt) {
            echo 'success';
        } else {
            echo 'error';
        }

        break;

        //invite
    case 'invite':
        sleep(1);
        $reponse = array(
            'message' => 'An error occurred',
            'status' => 0
        );
        $email = sanitize($_POST['email']);
        $dateTime = date("Y-m-d H:i:s");
        $admin = $_SESSION['id'];
        //checking if admin already exists
        $adminQuery = "SELECT * FROM hr_jobs_users WHERE email = ?";
        $stmt = $conn->prepare($adminQuery);
        $adminQueryExecute = $stmt->execute([
            $email
        ]);

        if (empty($email)) {
            $errors[] = "Admin email is required.";
            $reponse['message'] = 'Admin email is required.';
        } elseif ($stmt->rowCount() > 0) {
            $errors[] = "This email already exists!";
            $reponse['message'] = 'This email already exists!';
        } else {
            $invite_token = bin2hex(openssl_random_pseudo_bytes(40));

            //send invite link email
            require_once '../mailer/PHPMailer.php';
            require_once '../mailer/SMTP.php';

            $mail = new PHPMailer;
            $mail->IsSMTP();
        $mail->isHTML(true);
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'echo';
        $mail->Host = 'mail.apainsurance.org';
        $mail->SMTPAuth   = true;
        $mail->SMTPSecure = 'ssl';
        $mail->SMTPDebug = 0;
        $mail->Port = 465;
        $mail->Username = 'apa.website@apollo.co.ke';
        $mail->Password = 'Apa321$321';

            $mail->setFrom('apa.website@apollo.co.ke', 'APA HR ADMIN');
            $mail->addAddress($email);     // Add a recipient
            //$mail->addAddress('ellen@example.com');               // Name is optional
            $mail->addReplyTo('no-reply@apollo.co.ke', 'No reply');
            $mail->addCC('anthonybaru@gmail.com');

            //$mail->addCC("{$email}");
            //$mail->addCC("{$email}");
            $mail->Subject = 'INVITATION LINK.';

            // Program to display URL of current page.

            if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
                $link = "https";
            } else {
                $link = "http";
            }

            // Here append the common URL characters.
            $link .= "://";

            // Append the host(domain name, ip) to the URL.
            //$link .= $_SERVER['HTTP_HOST'];

            // Append the requested resource location to the URL
            //$link .= $_SERVER['SERVER_NAME'];

            $mail->Body    = 'You have been invited to be an Admin of APA INSURANCE HR System. Please click the link to create your account: </br>' . $link . $_SERVER['HTTP_HOST'] . '/hr/create_user.php?invite_token=' . $invite_token . '';

            if ($mail->send()) {
                //echo 'Email sent successfully to ' . $email;

                $query = "INSERT INTO hr_jobs_users(`created_at`,`invite_token`,`invited_by`, email) VALUES('$dateTime','$invite_token','$admin','$email')";
                $run = $conn->query($query);
                if ($run) {
                    //$_SESSION['SuccessMessage'] = 'Activation link sent successfully to: ' . $email . '.';
                    $reponse['message'] = 'success';
                    $response['status'] = 1;
                } else {
                    $response['message'] = "Something went wrong. Please try again!";
                }
            } else {
                $reponse['message'] = 'Something went wrong. Please try again. Activation link not sent : ' . $mail->ErrorInfo;
            }
            // } else {
            //     $_SESSION['ErroMessage'] = "Something went wrong. Please try again!";
            //     $response['message'] = "Something went wrong. Please try again!";
            // }
        }
        echo json_encode($reponse);
        break;

        //register
    case 'register':
        sleep(1);
        $response = array(
            'message' => 'An error occured',
            'status' => 0
        );
        $created_at = date('Y-m-d H:i:s');
        $first_name = sanitize($_POST['first_name']);
        $last_name = sanitize($_POST['last_name']);
        $password = sanitize($_POST['password']);
        $confirm = sanitize($_POST['confirm']);
        $invite_token = sanitize($_POST['invite_token']);

        if (empty($first_name) || empty($last_name) || empty($password) || empty($invite_token)) {
            $errors[] = 'All fields are required.';
            $response['message'] = 'All fields are required.';
        } else {
            if (strlen($password) < 6 || strlen($confirm) < 6) {
                $response['message'] = 'Password can\'t be less than 6 characters!';
                $errors[] = 'error';
            } else {
                if ($password !== $confirm) {
                    $response['message'] = 'Passwords don\'t match!';
                    $errors[] = 'error';
                }

                if (empty($errors)) {
                    $password =  password_hash($password, PASSWORD_DEFAULT);
                    $insertquery  = "UPDATE hr_jobs_users SET first_name = ?, last_name=?, `password`=?, created_at=? , invite_token = ? WHERE invite_token = ? ";
                    $stmt = $conn->prepare($insertquery);
                    $executeInsert = $stmt->execute([
                        $first_name, $last_name, $password, $created_at, 'Registered', $invite_token,
                    ]);
                    if ($executeInsert) {
                        $response['message'] = 'success';
                        $response['status'] = 1;
                    } else {
                        $response['message'] = 'An error occurred while registering. Please try again!';
                    }
                } else {
                    $response['message'] = 'An error occurred. Please try again!';
                }
            }
        }


        echo json_encode($response);
        break;

        // login

    case 'login':
        sleep(1);
        $email = sanitize($_POST['email']);
        $password = sanitize($_POST['password']);
        $login_details = $conn->prepare("SELECT * FROM hr_jobs_users WHERE email= ? LIMIT 1 ");
        $login_details->execute([$email]);
        $check = $login_details->fetchAll();


        if (empty($email) || empty($password)) {
            $errors[] = 'All fields are required.';
            echo 'All fields are required.';
        } else {
            if ($login_details->rowCount() !== 1) {
                $errors[] = 'Invalid email.';
                echo  'Invalid email.';
                exit;
            } else {
                foreach ($check as $d) {
                    $passworddb = $d['password'];
                }
                $hashPwdCheck = password_verify($password, $passworddb);
                if ($hashPwdCheck == false) {
                    $errors[] = 'Invalid password.';
                    echo 'Invalid password.';
                    exit;
                } else {
                    if ($hashPwdCheck == true) {
                        if (empty($errors)) {
                            $_SESSION['first_name'] = $d['first_name'];
                            $_SESSION['last_name'] = $d['last_name'];
                            $_SESSION['id'] = $d['id'];
                            echo 'success';
                            //$_SESSION['successMessage'] = 'Welcome ' . $_SESSION['fname'] . '.';
                            //         echo "<script>
                            // window.open('index.php','_SELF');
                            // </script>";
                            exit();
                        } else {
                            echo 'An error occurred! Please try again.';
                        }
                    }
                }
            }
        }
        break;

    case 'logout':
        sleep(1);
        session_destroy();
        redirect_to('../login.php');
        break;


    case 'reset_link':
        sleep(1);
        $email = sanitize($_POST['email']);
        if (empty($email)) {
            $errors[] = 'Email field is required';
            echo 'Email field is required';
            exit;
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors[] = 'Invalid email!';
            echo 'Invalid email!';
            exit;
        }

        if (empty($errors)) {
            // email exists in db
            $emailquery = $conn->prepare("SELECT * FROM hr_jobs_users WHERE email = ? LIMIT 1");
            $emailquery->execute([$email]);
            $name = $emailquery->fetchAll();
            foreach ($name as $d) {
                $username  = $d['first_name'] . ' ' . $d['last_name'];
            }

            if ($emailquery->rowCount() != 1) {
                $errors[] = 'The email you entered does not exist in our database!';
                echo 'The email you entered does not exist in our database!';
                exit;
            } else {
                // generate token
                $token = bin2hex(openssl_random_pseudo_bytes(40));
                //insert token to db
                $updateToken = $conn->prepare("UPDATE hr_jobs_users SET password_reset_token = ? WHERE email = ? ");
                $updateToken->execute([$token, $email]);
                //send reset email
                require_once $base . '/mailer/PHPMailer.php';
                require_once $base . '/mailer/SMTP.php';

                $mail = new PHPMailer;
                $mail->IsSMTP();
        $mail->isHTML(true);
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'echo';
        $mail->Host = 'mail.apainsurance.org';
        $mail->SMTPAuth   = true;
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;
        $mail->Username = 'apa.website@apollo.co.ke';
        $mail->Password = 'Apa321$321';                                   // TCP port to connect to

               $mail->setFrom('apa.website@apollo.co.ke', 'APA HR ADMIN');
            $mail->addAddress($email);     // Add a recipient
            //$mail->addAddress('ellen@example.com');               // Name is optional
            $mail->addReplyTo('no-reply@apollo.co.ke', 'No reply');
            $mail->addCC('anthonybaru@gmail.com');
                //$mail->addCC("{$email}");
                //$mail->addCC("{$email}");
                $mail->Subject = 'Reset Account Link.';

                // Program to display URL of current page.

                if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
                    $link = "https";
                } else {
                    $link = "http";
                }

                // Here append the common URL characters.
                $link .= "://";

                // Append the host(domain name, ip) to the URL.
                //$link .= $_SERVER['HTTP_HOST'];

                // Append the requested resource location to the URL
                //$link .= $_SERVER['SERVER_NAME'];

                $mail->Body    = 'Please click the link to reset your password: ' . $link . $_SERVER['HTTP_HOST'] . '/hr/reset_password.php?password_reset_token=' . $token . '';

                if ($mail->send()) {
                    echo 'success';
                // $_SESSION['SuccessMessage'] = 'Reset link sent successfully. Check your email (' . $usernameEmail . ').';
                } else {
                    echo 'Something went wrong. Please try again. Activation link not sent.';
                }
            }
        }
        break;

    case 'reset_password':
        sleep(1);
        $password = sanitize($_POST['password']);
        $confirm = sanitize($_POST['confirm']);
        $reset_token = sanitize($_POST['token']);
        $validtq = "SELECT password_reset_token FROM hr_jobs_users WHERE password_reset_token=  ? LIMIT 1";
        $validstmt = $conn->prepare($validtq);
        $validstmt->execute([$reset_token]);
        $count = $validstmt->rowCount();
        if ($count != 1) {
            $errors[] = 'Invalid request';
            echo 'Invalid request';
            exit;
        }

        if (empty($password) || empty($confirm)) {
            $errors[] = 'All fields are required';
            echo 'All fields are required';
            exit;
        }
        if ($password !== $confirm) {
            $errors[] = 'Password don\'t match';
            echo 'Password don\'t match';
            exit;
        }
        if (strlen($password) < 6 || strlen($confirm) < 6) {
            $errors[] = 'Password can\'t be less than 6 characters ';
            echo 'Password can\'t be less than 6 characters ';
            exit;
        }

        if (empty($errors)) {
            $password = password_hash($password, PASSWORD_DEFAULT);
            $updateq = "UPDATE hr_jobs_users SET  `password` = ? WHERE password_reset_token = ? ";
            $updatestmt = $conn->prepare($updateq);
            $updatestmt->execute([
                $password, $reset_token
            ]);
            if ($updatestmt) {
                echo 'success';
            } else {
                echo 'An error occurred';
            }
        }
        break;
    default:
        # code...
        break;
}
