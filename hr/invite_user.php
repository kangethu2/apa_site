<?php
require_once('config/db.php');
require_once('inc/functions.php');
require_once('inc/sessions.php');
confirm_login();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HR RECRUITMENT PORTAL</title>

    <?php require_once('inc/head_links.php'); ?>

</head>

<body class="bg-gradient-primary">
    <br>
    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4"><b>Enter email of user</b></h1>
                            </div><br>
                            <?php
                            if (!empty($errors)) {
                                echo display_errors($errors);
                            }
                            echo errorMessage();
                            echo successMessage();
                            ?>
                            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" class="user" id="form">
                                <div class="form-group">
                                    <input name="email" type="email" class="form-control form-control-user" id="email" placeholder="Email Address" required>
                                </div>
                                <button type="submit" id="submit" href="user_database.html" class="btn btn-primary btn-user btn-block">
                                    Send Link
                                </button>
                                <hr>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php require_once('inc/js.php'); ?>
    <script>
        $(document).ready(function() {
            $('#form').parsley();
            $('#form').on('submit', function(e) {
                e.preventDefault();
                var dataString = $(this).serialize();
                var form = $(this);
                $.ajax({
                    type: "POST",
                    url: "ajax/processor.php?request=invite",
                    data: dataString,
                    dataType: "json",
                    beforeSend: function() {
                        $('.btn').attr("disabled", true).html("Sending");
                    },
                    success: function(response) {
                        if (response.message == 'success') {
                            form[0].reset();
                            swal.fire({
                                title: 'Invite email sent successfully.',
                                type: 'success',
                                allowOutsideClick: false,
                                showConfirmButton: true,
                                showCloseButton: true,
                            });
                            // setTimeout(function() {
                            //     window.location.href = "login.php";
                            // }, 3000);
                        } else {
                            swal.fire({
                                title: response.message,
                                type: 'error',
                                allowOutsideClick: true,
                                showConfirmButton: true
                            });
                        }
                        $('.btn').attr("disabled", false).html('Send Link');
                    }
                });
            });
        });
    </script>
</body>

</html>