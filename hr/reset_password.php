<?php
require_once('config/db.php');
require_once('inc/functions.php');
require_once('inc/sessions.php');
if (!isset($_GET['password_reset_token']) || empty($_GET['password_reset_token'])) {
    $_SESSION['errorMessage'] = 'Invalid access';
    redirect_to('login.php');
} else {
    $password_reset_token = $_GET['password_reset_token'];
    $_SESSION['password_reset_token'] = $_GET['password_reset_token'];
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HR RECRUITMENT PORTAL</title>

    <?php require_once('inc/head_links.php'); ?>

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">
            <div class="col-xl-10 col-lg-12 col-md-9">

                <br><br><br>

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Reset</h1><br>
                                    </div>
                                    <?php
                                    if (!empty($errors)) {
                                        echo display_errors($errors);
                                    }
                                    echo errorMessage();
                                    echo successMessage();
                                    ?>
                                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" class="user" id="form">
                                        <div class="form-group">
                                            <input name="password" type="password" class="form-control form-control-user" id="password" aria-describedby="password" placeholder="Enter New Password" data-parsley-trigger="keyup" required>
                                        </div>
                                        <div class="form-group">
                                            <input name="confirm" type="password" class="form-control form-control-user" d="confirm" placeholder="Confirm password" data-parsley-equalto='#password' data-parsley-trigger="keyup" required>
                                        </div>
                                        <input type="hidden" name="token" value="<?php echo $_GET['password_reset_token'] ?>">

                                        <div class="form-group">
                                            <!-- <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck"><br>
                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                      </div> -->
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-user btn-block">
                                            Reset Password
                                        </button>
                                        <hr><br>
                                        <!-- <a href="index.html" class="btn btn-google btn-user btn-block">
                      <i class="fab fa-google fa-fw"></i> Login with Google
                    </a>
                    <a href="index.html" class="btn btn-facebook btn-user btn-block">
                      <i class="fab fa-facebook-f fa-fw"></i> Login with Facebook
                    </a> -->
                                    </form>

                                    <!-- <hr> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <?php echo require_once('inc/js.php'); ?>
    <script>
        $('#form').parsley();
        $('#form').on('submit', function(e) {
            e.preventDefault();
            var dataString = $('#form').serialize();
            $.ajax({
                type: "POST",
                url: "ajax/processor.php?request=reset_password",
                data: dataString,
                dataType: 'text',
                success: function(response) {
                    if (response == 'success') {
                        $('#form')[0].reset();
                        swal.fire({
                            title: 'Password updated successfully. Please login.',
                            type: response,
                            allowOutsideClick: false,
                            showConfirmButton: false
                        });
                        setTimeout(function() {
                            window.location.href = "login.php";
                        }, 3000);
                    } else {
                        swal.fire({
                            title: response,
                            type: 'error',
                            allowOutsideClick: true,
                            showConfirmButton: true
                        });
                    }
                }
            });
        });
    </script>
</body>

</html>