<?php
require_once('config/db.php');
require_once('inc/functions.php');
require_once('inc/sessions.php');

// getting the live posts
$query = "SELECT * FROM apa_job_posts WHERE archive = 0 ORDER BY date_added DESC ";
$stmt = $conn->query($query);
$livepost = $stmt->fetchAll();

// hiding live post
if (isset($_GET['archive'])) {
    $archive_id = sanitize($_GET['archive']);
    if (isset($_GET['archive']) && empty($archive_id)) {
        $errors[] = 'An error occurred. Please try again!';
        //redirect_to($_SERVER['PHP_SELF']);
    }
    if (empty($errors)) {
        $query = "UPDATE apa_job_posts SET archive = 1 WHERE job_id=? ";
        $update = $conn->prepare($query)->execute([$archive_id]);
        if ($update) {
            $_SESSION['successMessage'] = 'Post archived successfully!';
            redirect_to($_SERVER['PHP_SELF']);
        } else {
            $_SESSION['errorMessage'] = 'An error occurred. Please try again!';
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HR RECRUITMENT PORTAL</title>

    <?php require_once 'inc/head_links.php'; ?>


</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php
        $page = basename($_SERVER['PHP_SELF']);
        require_once 'views/sidebar.php';
        ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php require_once 'views/nav.php'; ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container live text-center">
                    <br>
                    <h2>LIVE POSTS</h2>
                    <?php
                    if (!empty($errors)) {
                        echo display_errors($errors);
                    }
                    echo errorMessage();
                    echo successMessage();
                    ?>
                    <div class="row">
                        <?php if (count($livepost) > 0) : ?>
                            <?php foreach ($livepost as $d) : ?>
                                <div class="col-6">
                                    <div class="card box-ap mb-4 py-1 border-bottom-primary1">
                                        <div class="card-body illustrat-icon text-center">
                                            <h5><?php echo strtoupper($d['job_title']); ?></h5>
                                            <hr>
                                            <br>
                                            <ul class="text-left">
                                                <li><b>DEPARTMENT</b> : <?php echo ucwords($d['department']) ?></li>
                                                <li><b>REPORTS TO</b> : <?php echo ucwords($d['reports_to']) ?></li>
                                                <li><b>LOCATION</b> : <?php echo (($d['location']) ?  ucfirst($d['location']) : ''); ?></li>
                                                <li><b>EMPLOYMENT TYPE</b> : <?php echo ucfirst($d['employment_type']) ?></li>
                                            </ul>

                                            <div class="row btn-sub">
                                                <div class="col-4">
                                                    <a href="applicant_table.php?view=<?php echo $d['job_id'] ?>" class="btn btn-primary">
                                                        View applicants
                                                    </a>
                                                </div>

                                                <div class="col-4">
                                                    <a href="edit_post.php?edit=<?php echo $d['job_id']; ?>&live" class="btn btn-dark">
                                                        Edit post
                                                    </a>
                                                </div>

                                                <div class="col-4">
                                                    <a href="live_post.php?archive=<?php echo $d['job_id']; ?>" class="btn btn-danger">
                                                        Archive post
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>

                        <?php else : ?>
                            <div class="col-6">
                                <p>No posts to show.</p>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <!-- /.container-fluid -->


            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php require_once 'views/footer.php'; ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.php">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <?php require_once('inc/js.php'); ?>
    <script>
        $(document).ready(function() {

        });
    </script>
</body>

</html>