<?php
require_once('config/db.php');
require_once('inc/functions.php');
require_once('inc/sessions.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HR RECRUITMENT PORTAL</title>
    <?php include 'inc/head_links.php'; ?>
    <script src="ckeditor5-build-classic/ckeditor.js"></script>



</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php
        $page = basename($_SERVER['PHP_SELF']);
        include 'views/sidebar.php';
        ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include 'views/nav.php'; ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid ">

                    <div class="row center-box">
                        <div class="col-10 box-container ">
                            <h2>CREATE POST</h2>
                            <form id="create_post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">

                                <!--===== TAGS ===== -->
                                <fieldset>
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input name="title" type="text" class="form-control" id="title" placeholder="" required>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="department">Company</label>
                                                <select name="company" id="company" class="form-control" required>
                                                    <option selected disabled>--please choose--</option>
                                                    <option value="Apollo Group">Apollo Group (Shared service)</option>
                                                    <option value="APA Insurance">APA Insurance</option>
                                                    <option value="APA Life">APA Life</option>
                                                    <option value="Apollo Asset Managment">Apollo Asset Managment</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="department">Department</label>
                                                <input name="department" type="text" class="form-control" id="department" placeholder="" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="employment_type">Employment type</label>
                                                <select name="employment_type" id="employment_type" class="form-control" required>
                                                    <option selected disabled>--please choose--</option>
                                                    <option value="Permanent">Permanent</option>
                                                    <option value="Contract">Contract</option>
                                                    <option value="Internship">Internship</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="deadline">Deadline</label>
                                                <input name="deadline" type="date" class="form-control" id="deadline" placeholder="" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="salary">Salary (optional)</label>
                                                <input name="salary" type="text" class="form-control value" id="salary" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="reports_to">Reports to</label>
                                                <input name="reports_to" type="text" class="form-control" id="reports_to" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="location">Location</label>
                                                <input name="location" type="text" class="form-control" id="location" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="email">Receiving email</label>
                                                <input name="email" type="text" class="form-control" id="email" placeholder="" required>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                                <br>
                                <hr>
                                <!--===== DESCRIPTION ===== -->

                                <fieldset>
                                    <h2>DESCRIPTION</h2><br>
                                    <div class="form-group">
                                        <label for="job_short_description"><B>JOB SHORT DESCRIPTION</B> </label>
                                        <textarea name="job_short_description" class="form-control" id="editor0" rows="10" required></textarea>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="key_primary_responsibilities"><B>KEY PRIMARY RESPONSIBILITIES </B> </label>
                                        <textarea name="key_primary_responsibilities" class="form-control" id="editor" rows="10" required></textarea>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="academic_qualifications"><B>ACADEMIC QUALIFICATIONS </B> </label>
                                        <textarea name="academic_qualifications" class="form-control" id="editor1" rows="10" required></textarea>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="professional_qualification"><B>PROFESSIONAL QUALIFICATION </B> </label>
                                        <textarea name="professional_qualification" class="form-control" id="editor2" rows="10" required></textarea></textarea> </div> <br>
                                    <div class="form-group">
                                        <label for="job_skills_and_requirements"><B>JOB SKILLS AND REQUIREMENTS </B></label>
                                        <textarea name="job_skills_and_requirements" class="form-control" id="editor3" rows="10" required></textarea>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="experience"><B>EXPERIENCE </B></label>
                                        <textarea name="experience" class="form-control" id="editor4" rows="10" required>
                                        </textarea>
                                    </div>
                                </fieldset>
                                <br><br>
                                <div class="btn-post text-center">
                                    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- QUICK MENU -->

                </div>
                <!-- /.container-fluid -->


            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php include 'views/footer.php'; ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!--PREVIEW  MODAL -->
    <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalScrollableTitle">PREVIEW OF POST</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid12">

                        <div class="row ">
                            <div class="col-11 job-box">
                                <div class="job-description text-center"><br>
                                    <h2>BUSINESS INTELLIGENCE OFFICER</h2><br>
                                    <hr>
                                    <ul class="text-left">
                                        <li><b>DEPARTMENT:</b> <span>ICT</span> </li>
                                        <li><b>REPORTS TO:</b> <span> Group CIO</span> </li>
                                        <li><b>LOCATION:</b> <span>Nairobi,Kenya</span> </li>
                                        <li><b>EMPLOYMENT TYPE:</b> <span>Full Time</span> </li>
                                        <li><b>JOB ID:</b> <span>223759BR</span> </li>
                                    </ul>
                                    <hr>
                                    <div class="job-content">
                                        <div class="text-justify"><br>
                                            <h3>KEY PRIMARY RESPONSIBILITIES</h3>
                                            <p> 1) Business requirements & analysis</p>
                                            <ul>
                                                <li>Consulting with end users / Management on business needs, translating business
                                                    needs into analytics/ reporting requirements
                                                </li>
                                                <li>Interprets business requirements and determines optimum BI solutions to meet
                                                    business needs.
                                                </li>
                                                <li>Advises executives on how BI (processes, practices and technologies) play a
                                                    critical role in improving business management and optimization
                                                </li>
                                                <li>Researches business problems and creates models that help analyse these
                                                    business problems.
                                                </li>
                                                <li>Collaborating with end users to identify needs and opportunities for
                                                    improved data management and analysis.
                                                </li>
                                                <li>Developing, packaging and delivering new service offerings related to
                                                    business intelligence, encompassing clearly defined value propositions and
                                                    creating realistic profitability models
                                                </li>
                                            </ul>
                                            <p> 2) Data visualization</p>
                                            <ul>
                                                <li>Develop and improve custom data visualization and analytics tools to help
                                                    various teams within the group identify trends and best practices, and support
                                                    in their monitoring of monthly and annual goals
                                                </li>
                                                <li>Develop business intelligence reports – including interactive business
                                                    intelligence reports, strategic management reports and ad hoc reports – on a
                                                    prioritised basis
                                                </li>
                                                <li>coordinating the ongoing development and operations of a business intelligence
                                                    architecture that enables fact-based decision-making, ad hoc analysis and
                                                    insight generation
                                                </li>
                                                <li>To advise generally on the potential for data use across the business, and also
                                                    to make colleagues aware of any opportunities presented by the datasets for
                                                    cross selling.
                                                </li>
                                                <li>Providing regular analysis and interpretation of data and trends to support
                                                    decision making by the management team, including text, graphics and other
                                                    statistical representations
                                                </li>
                                                <li>Creation of dashboards for presentation of accurate information to Management at
                                                    different levels and locations
                                                </li>
                                            </ul>
                                            <p> 3) Information quality assurance</p>
                                            <ul>
                                                <li>Overseeing development of an enterprise data warehouse as part of Group wide
                                                    digital transformation strategy
                                                </li>
                                                <li>Improving and streamlining processes regarding data flow and data quality to
                                                    improve information confidentiality, integrity and availability
                                                </li>
                                            </ul>
                                            <hr>
                                            <h3>ACADEMIC QUALIFICATIONS</h3>
                                            <ul>
                                                <li>A relevant qualification, e.g. finance, statistics, economics, business studies,
                                                    information technology or equivalent expertise.
                                                </li>
                                                <li>Relevant professional qualification from a recognized institution.
                                                </li>
                                            </ul>
                                            <hr>
                                            <h3>JOB SKILLS AND REQUIREMENTS</h3>
                                            <ul>
                                                <li>Proficiency in IBM Cognos and Oracle Business Intelligence. Other platforms an
                                                    added advantage
                                                </li>
                                                <li>Proficiency in SQL
                                                </li>
                                                <li>Technical Skills: use of BI tools to mine data sources and look for trends
                                                </li>
                                                <li>Strong Analytical Skills: determining what data trends mean. Being able to
                                                    analyze the data is crucial
                                                </li>
                                                <li>Communication Skills: Communicate their findings to the company and/or managers
                                                </li>
                                                <li>Problem-solving: Recommend solutions for creating more revenue and reducing loss
                                                </li>
                                                <li>Time Management: work on large projects with many data streams and many
                                                    deadlines. Being able to time manage is crucial.
                                                </li>

                                            </ul>
                                            <hr>
                                            <h3>EXPERIENCE</h3>
                                            <ul>
                                                <li>A Business intelligence experience with 4 years working experience in a similar
                                                    role at a similar level.
                                                </li>
                                                <li>Track record of substantial success in a similar role at a similar level. With
                                                    experience of current best practice and up-to-date ideas on the maintenance and
                                                    presentation of ‘mission critical’ business information for executive teams
                                                </li>
                                                <li>Significant experience of numerical, financial or other analysis and
                                                    interpretative techniques
                                                </li>
                                                <li>Experience of producing high level graphical representations of data in an
                                                    insightful and user friendly format and an understanding of the best way to
                                                    communicate analytical information
                                                </li>
                                            </ul>
                                            <br><br>

                                        </div>

                                    </div>
                                </div><BR>
                            </div>
                        </div>

                        <!-- QUICK MENU -->

                    </div>
                </div>
                <div class=" modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <div class="btn-post">
                        <button type="button" class="btn btn-primary">Publish</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php require_once('inc/js.php'); ?>


    <script>
        $(document).ready(function() {
            // ckeditor 5
            ClassicEditor
                .create(document.querySelector('#editor0'))
                .catch(error => {
                    console.error(error);
                });
            ClassicEditor
                .create(document.querySelector('#editor'))
                .catch(error => {
                    console.error(error);
                });

            ClassicEditor
                .create(document.querySelector('#editor1'))
                .catch(error => {
                    console.error(error);
                });

            ClassicEditor
                .create(document.querySelector('#editor2'))
                .catch(error => {
                    console.error(error);
                });

            ClassicEditor
                .create(document.querySelector('#editor3'))
                .catch(error => {
                    console.error(error);
                });

            ClassicEditor
                .create(document.querySelector('#editor4'))
                .catch(error => {
                    console.error(error);
                });

            $('#create_post').parsley();
            $('#create_post').on('submit', function(e) {
                e.preventDefault();
                var dataString = $('#create_post').serialize();
                console.log(dataString);
                $.ajax({
                    type: "POST",
                    url: "ajax/processor.php?request=create_post",
                    data: dataString,
                    beforeSend: function() {
                        $('.btn').attr("disabled", true).html("Submitting");
                    },
                    success: function(response) {
                        if (response == 'success') {
                            $('#create_post')[0].reset();
                            Swal.fire({
                                title: 'Job has been posted successfully.',
                                type: response,
                                allowOutsideClick: false,
                                showConfirmButton: false
                            });
                            setTimeout(function() {
                                window.location.href = "archive_post.php";
                            }, 2000);
                        } else {
                            swal.fire('ERROR', 'An error occurred. Please try again.', response);
                        }
                        $('.btn').attr("disabled", false).html('Submit');

                    }
                });
            });

            // number formatter
            $('#value, .value').keyup(function(event) {
                // skip for arrow keys
                if (event.which >= 37 && event.which <= 40) return;

                // format number
                $(this).val(function(index, value) {
                    return value
                        .replace(/\D/g, "")
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                });
            });
        });
    </script>

</body>

</html>