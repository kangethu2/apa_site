<?php
require_once('config/db.php');
require_once('inc/functions.php');
require_once('inc/sessions.php');
if (!isset($_GET['invite_token']) || empty($_GET['invite_token'])) {
    $_SESSION['errorMessage'] = 'Invalid selection!';
    redirect_to('login.php');
} else {
    $invite_token = $_GET['invite_token'];
    $query = "SELECT invite_token FROM hr_jobs_users WHERE invite_token = ? ";
    $stmt = $conn->prepare($query);
    $stmt->execute([
        $invite_token
    ]);
    if ($stmt->rowCount() !== 1) {
        $_SESSION['errorMessage'] = 'Invalid selection!';
        redirect_to('login.php');
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HR RECRUITMENT PORTAL</title>

    <?php require_once('inc/head_links.php'); ?>

</head>

<body class="bg-gradient-primary">
    <br>
    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4"><b>Create Account!</b></h1>
                            </div><br>
                            <?php
                            if (!empty($errors)) {
                                echo display_errors($errors);
                            }
                            echo errorMessage();
                            echo successMessage();
                            ?>
                            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" class="user" id="form">

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input name="first_name" type="text" class="form-control form-control-user" id="first_name" placeholder="First Name" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input name="last_name" type="text" class="form-control form-control-user" id="last_name" placeholder="Last Name" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input name="password" type="password" class="form-control form-control-user" id="password" placeholder="Password" required>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="confirm" type="password" class="form-control form-control-user" id=" confirm" placeholder="Confirm Password" required data-parsley-equalto="#password">
                                    </div>
                                    <div class="form-group">
                                        <input name="invite_token" type="hidden" class="form-control form-control-user" id="invite_token" value="<?php echo $invite_token; ?>" required>
                                    </div>
                                </div><br>
                                <button type="submit" id="submit" href="user_database.html" class="btn btn-primary btn-user btn-block">
                                    Register
                                </button>
                                <hr>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php require_once('inc/js.php'); ?>
    <script>
        $(document).ready(function() {
            $('#form').parsley();
            $('#form').on('submit', function(e) {
                e.preventDefault();
                var dataString = $(this).serialize();
                var form = $(this);
                $.ajax({
                    type: "POST",
                    url: "ajax/processor.php?request=register",
                    data: dataString,
                    dataType: "json",
                    beforeSend: function() {
                        $('.btn').attr("disabled", true).html("Registering");
                    },
                    success: function(response) {
                        if (response.message == 'success') {
                            form[0].reset();
                            swal.fire({
                                title: 'Account updated successfully. You will now be redirected to the login page.',
                                type: response,
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                showCloseButton: true
                            });
                            setTimeout(function() {
                                window.location.href = "login.php";
                            }, 3000);
                        } else {
                            swal.fire({
                                title: response.message,
                                type: 'error',
                                allowOutsideClick: true,
                                showConfirmButton: true,
                                showCloseButton: true
                            });
                        }
                        $('.btn').attr("disabled", false).html('Register');
                    }
                });
            });
        });
    </script>
</body>

</html>