<?php
require_once('config/db.php');
require_once('inc/functions.php');
require_once('inc/sessions.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HR RECRUITMENT PORTAL</title>

    <?php require_once 'inc/head_links.php'; ?>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php
        $page = basename($_SERVER['PHP_SELF']);
        require_once 'views/sidebar.php';
        ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php require_once 'views/nav.php'; ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <?php
                    // Dashboard
                    // livepost
                    $query = 'SELECT * FROM apa_job_posts WHERE  archive = 0 ';
                    $livestmt =  $conn->query($query);
                    //number of applicants
                    $applicants = 'SELECT * FROM apa_job_applicants';
                    $applicantstmt = $conn->query($applicants);

                    //expired job posts
                    $expiredq = 'SELECT * FROM apa_job_posts WHERE  archive = 1 ';
                    $expiredstmt = $conn->query($expiredq);
                    //total job posts
                    $query = "SELECT * FROM apa_job_posts";
                    $totaljobstmt = $conn->query($query);

                    ?>
                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-600">Dashboard</h1>
                        <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-success shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-sm-center font-weight-bold text-success text-uppercase mb-2">
                                                NUMBER OF APPLICANTS</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $applicantstmt->rowCount(); ?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-sm-center font-weight-bold text-primary text-uppercase mb-2">
                                                NUMBER OF LIVE POST
                                            </div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $livestmt->rowCount(); ?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-sm-center font-weight-bold text-info text-uppercase mb-2">
                                                EXPIRED JOB POST</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $expiredstmt->rowCount(); ?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Pending Requests Card Example -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-warning shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-sm-center font-weight-bold text-warning text-uppercase mb-2">
                                                job posts</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $totaljobstmt->rowCount(); ?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Content Row -->


                    <!-- QUICK MENU -->
                    <BR>
                    <div class="headline text-center">
                        <!-- <h2> QUICK ACCESS</h2> -->
                    </div>

                    <br>
                    <div class="container wrapper">
                        <div class="row">
                            <div class="col-md-3 box-card">
                                <a href="create_post.php">
                                    <div class="card box-cl mb-4 py-1 border-bottom-primary">
                                        <div class="card-body illustrat-icon text-center">
                                            <img src="img/post.png" alt="">
                                            <h5>Create post</h5>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3 box-card">
                                <a href="live_post.php">
                                    <div class="card box-cl mb-4 py-1 border-bottom-primary">
                                        <div class="card-body illustrat-icon text-center">
                                            <img src="img/application.png" alt="">
                                            <h5>View application</h5>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-md-3 box-card">
                                <a href="archive_post.php">
                                    <div class="card box-cl mb-4 py-1 border-bottom-primary">
                                        <div class="card-body illustrat-icon text-center">
                                            <img src="img/remove.png" alt="">
                                            <h5>Archived post</h5>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-md-3 box-card">
                                <a href="approved_candidate_post.php">
                                    <div class="card box-cl mb-4 py-1 border-bottom-primary">
                                        <div class="card-body illustrat-icon text-center">
                                            <img src="img/approved.png" alt="">
                                            <h5>Approved candidates</h5>
                                        </div>
                                    </div>
                                </a>
                            </div>


                        </div>

                        <div class="row">
                            <div class="col-md-3 box-card">
                                <a href="applicant_database.php">
                                    <div class="card box-cl mb-4 py-1 border-bottom-primary">
                                        <div class="card-body illustrat-icon text-center">
                                            <img src="img/apllicant.png" alt="">
                                            <h5>Applicant database</h5>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3 box-card">
                                <a href="create_user.php">
                                    <div class="card box-cl mb-4 py-1 border-bottom-primary">
                                        <div class="card-body illustrat-icon text-center">
                                            <img src="img/adduser.png" alt="">
                                            <h5>Create user</h5>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-md-3 box-card">
                                <a href="user_database.php">
                                    <div class="card box-cl mb-4 py-1 border-bottom-primary">
                                        <div class="card-body illustrat-icon text-center">
                                            <img src="img/profile.png" alt="">
                                            <h5>Users database</h5>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-md-3 box-card">
                                <a href="">
                                    <div class="card box-cl mb-4 py-1 border-bottom-primary">
                                        <div class="card-body illustrat-icon text-center">
                                            <img src="img/logout.png" alt="">
                                            <h5>Communication</h5>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->


            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php require_once 'views/footer.php'; ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>


    <?php require_once('inc/js.php'); ?>

</body>

</html>