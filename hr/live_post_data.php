<?php
require_once('config/db.php');
require_once('inc/functions.php');
require_once('inc/sessions.php');

// getting the live posts
$query = "SELECT * FROM apa_job_posts WHERE archive = 0";
$stmt = $conn->query($query);
$livepost = $stmt->fetchAll();

// hiding live post
if (isset($_GET['archive'])) {
    $archive_id = sanitize($_GET['archive']);
    if (isset($_GET['archive']) && empty($archive_id)) {
        $errors[] = 'An error occurred. Please try again!';
        //redirect_to($_SERVER['PHP_SELF']);
    }
    if (empty($errors)) {
        $query = "UPDATE apa_job_posts SET archive = 1 WHERE job_id=? ";
        $update = $conn->prepare($query)->execute([$archive_id]);
        if ($update) {
            $_SESSION['successMessage'] = 'Post archived successfully!';
            redirect_to($_SERVER['PHP_SELF']);
        } else {
            $_SESSION['errorMessage'] = 'An error occurred. Please try again!';
        }
    }
}
?>


<?php if (count($livepost) > 0) : ?>
    <?php foreach ($livepost as $d) : ?>
        <div class="col-6">
            <div class="card box-ap mb-4 py-1 border-bottom-primary1">
                <div class="card-body illustrat-icon text-center">
                    <h5><?php echo strtoupper($d['job_title']); ?></h5>
                    <hr>
                    <br>
                    <ul class="text-left">
                        <li><b>DEPARTMENT</b> : <?php echo ucwords($d['department']) ?></li>
                        <li><b>REPORTS TO</b> : <?php echo ucwords($d['reports_to']) ?></li>
                        <li><b>LOCATION</b> : <?php echo (($d['location']) ?  ucfirst($d['location']) : ''); ?></li>
                        <li><b>EMPLOYMENT TYPE</b> : <?php echo ucfirst($d['employment_type']) ?></li>
                    </ul>

                    <div class="row btn-sub">
                        <div class="col-4">
                            <a href="applicant_table.php?view=<?php echo $d['job_id'] ?>" class="btn btn-primary">
                                View applicants
                            </a>
                        </div>

                        <div class="col-4">
                            <a href="edit_post.php?edit=<?php echo $d['job_id']; ?>&live" class="btn btn-dark">
                                Edit post
                            </a>
                        </div>

                        <div class="col-4">
                            <a id="archive" href="" class="btn btn-danger">
                                Archive post
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                $('#archive').on('click', function(e) {
                    e.preventDefault();
                    $.ajax({
                        type: "POST",
                        url: "ajax/processor.php?request=archive",
                        data: "data",
                        dataType: "dataType",
                        success: function(response) {

                        }
                    });

                });
            });
        </script>
    <?php endforeach; ?>

<?php else : ?>
    <div class="col-6">
        <p>No posts to show.</p>
    </div>
<?php endif; ?>