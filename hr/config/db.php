<?php
if ($_SERVER['DOCUMENT_ROOT'] == 'C:/xampp/htdocs') :
    $servername = "localhost";
    $username = "root";
    $password = "";
else :
    $servername = "localhost";
    $username = "root";
    $password = "Apa321$321";
endif;

try {
    $conn = new PDO("mysql:host=$servername;dbname=apa_website", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected successfully";
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

//root directory
$base = $_SERVER['DOCUMENT_ROOT'] . '/hr';

//timezone
date_default_timezone_set("Africa/Nairobi");
