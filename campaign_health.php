<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title> HEALTH COVER - APA INSURANCE </title>

    <!-- META DATA DESCRIPTION FOR GOOGLE SEARCH RESULT -->
    <meta name="description" content="A healthy family is a happy family. This is why we 
    have medical insurance products designed to cover individuals and families ">
    <meta name="keywords" content="Health cover, Jamii plus, Health Insurance, Afya Nafuu">
    <meta name="author" content="">

    <!-- FACEBOOK MEATADATA -->
    <meta property="og:url" content="https://www.apainsurance.org/campaign_health.php" />
    <meta property="og:type" content="Homepage" />
    <meta property="og:title" content="APA Insurance, APA Insurance Kenya, APA Insurance Limited" />
    <meta property="og:description" content="A healthy family is a happy family. This is why we 
    have medical insurance products designed to cover individuals and families" />

    <!-- STYLESHEET -->
    <link rel="stylesheet" href="css/apollo_centre.css" media="screen">
    <link rel="stylesheet" href="css/modal.css" media="screen">
    <link rel="stylesheet" href="css/product.css" media="screen">
    <link rel="stylesheet" href="css/parsley.css" media="screen">

    <?php include 'views/head_links.php'; ?>

</head>

<body>

    <!--==========================
    Header
    ============================-->
    <?php include 'views/nav.php'; ?>
    <!-- #header -->
<br><br>
<div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-top" uk-grid>
            <div class="uk-background-cover uk-visible@m" style="background-image: url('img/campaign/health.jpg'); background-size: contain;" uk-height-viewport></div>
            <div class="uk-padding-large">
            <br>
            <div class="form-container uk-hidden@m">
                <h3 >FAMILY HEALTH COVER</h3>
                <p>A healthy family is a happy family. With our health policy, you get stand-alone cover for children,it also covers term babies from 38 weeks to 
                80 years and comes with embedded travel cover.</p>
                <hr>
            </div>

                <form class="form-container" id="form-jp" action="<?= $_SERVER['PHP_SELF']; ?>" method="POST">
                    <h3 for="inputAddress" class="comp-detail">PERSONAL DETAILS</h3>
                    <div class="container">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="full_name">Full Name</label>
                                <input name="full_name" type="text" class="form-control" id="John Doe" placeholder="John Doe" value="" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="phone">Mobile Number</label>
                                <input name="phone" data-parsley-pattern="^(?:254|\+254|0)?(7(?:(?:[123456789][0-9])|(?:0[0-8])|(4[0-1]))[0-9]{6})$" data-parsley-trigger="keyup" type="tel" class="form-control" id="phone" placeholder="07xx 343 xx7" value="" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="email">Email Address</label>
                                <input name="email" type="email" class="form-control" id="email" placeholder="john@example.com" value="" required data-parsley-type="email" data-parsley-trigger="keyup">
                            </div>
                            <div class=" form-group col-md-6">
                                <label for="location">Location</label>
                                <input name="location" type="text" class="form-control" id="location" placeholder="Nairobi" value="" required>
                            </div>
                        </div>

                    </div>
                    <hr>
                    <!-- <div class="form-group col-md-12">
                            <h3 for="inputAddress">COVER DETAILS</h3>
                        </div> -->

                    <div class="container">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="inpatient"> Inpatient Benefit (Kshs)</label>
                                <select id="inpatient" name="inpatient" class="form-control selectFilter" data-target="section" required>
                                    <option value="-1" selected disabled>Choose...</option>
                                    <option value="10000000">Kshs 10,000,000 </option>
                                    <option value="5000000">Kshs 5,000,000 </option>
                                    <option value="2000000">Kshs 2,000,000 </option>
                                    <option value="1000000">Kshs 1,000,000 </option>
                                    <option value="500000">Kshs 500,000 </option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="age"> Your Age category</label>
                                <select id="age" name="age" class="form-control" required>
                                    <option value="-1" selected disabled>Select your age range...</option>
                                    <option value="20 - 24"> 20 - 24 yrs</option>
                                    <option value="24 - 25"> 25 - 29 yrs</option>
                                    <option value="30 - 34"> 30 - 34 yrs</option>
                                    <option value="35 - 39"> 35 - 39 yrs</option>
                                    <option value="40 - 44"> 40 - 44 yrs</option>
                                    <option value="45 - 49"> 45 - 49 yrs</option>
                                    <option value="50 - 54"> 50 - 54 yrs</option>
                                    <option value="55 - 59"> 55 - 59 yrs</option>
                                    <option value="60 - 64"> 60 - 64 yrs</option>
                                    <option value="65 - 69"> 65 - 69 yrs</option>
                                    <option value="70 - 75"> 70 - 75 yrs</option>
                                    <option value="76 - 80"> 76 - 80 yrs</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="number">Number of Children (Age < 20)</label> <input name="number" type="number" class="form-control" id="number" placeholder="3" value="">
                            </div>
                        </div>


                        <div style="display: none">
                            <input type="hidden" id="product_id" name="product_id" value="14">
                            <input type="hidden" id="product_category_id" name="product_category_id" value="14">
                        </div>
                        <div class="row book-btn">
                            <div class="col-md-12">
                                <button type="submit" name="request" class="btn btn-primary">SUBMIT <i class="fas fa-paper-plane"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <!-- =====================================FOOTER===================================== -->
    <?php include 'views/footer.php'; ?>
    <!-- #footer -->


    <?php include 'views/get_cover.php'; ?>


    <!-- loading scripts -->
    <?php
    require_once 'inc/scripts.php';
    ?>
    <script src="js/parsley.min.js"></script>
    <script src="js/lead.js"></script>
</body>

</html>