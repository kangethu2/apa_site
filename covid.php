<?php
require_once 'inc/db.php';
require_once 'inc/sessions.php';
require_once 'inc/functions.php';
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Covid19 - APA Insurance </title>

    <!-- META DATA DESCRIPTION FOR GOOGLE SEARCH RESULT -->
    <meta name="description" content="Welcome to APA Insurance, one of the largest insurers in East Africa.
    You can now purchase insurance, service your policies and report a claim online. 
    This gives you more time to do the things that you love most ">
    <meta name="keywords" content="insurance cover, apa insurance kenya, apa insurance limited, 
    buy insurance cover, buy cover online, cover quotation, insurance online quotation, apa online services">
    <meta name="author" content="">

    <!-- FACEBOOK MEATADATA -->
    <meta property="og:url" content="https://www.apainsurance.org/covid.php" />
    <meta property="og:type" content="Homepage" />
    <meta property="og:title" content="APA Insurance, APA Insurance Kenya, APA Insurance Limited" />
    <meta property="og:description" content="Welcome to APA Insurance, one of the largest insurers in East Africa.
    You can now purchase insurance, service your policies and report a claim online. 
    This gives you more time to do the things that you love most" />

    <!-- STYLESHEET -->
    <link rel="stylesheet" href="css/apollo_centre.css" media="screen">
    <link rel="stylesheet" href="css/modal.css" media="screen">
    <link rel="stylesheet" href="css/product.css" media="screen">
    <link rel="stylesheet" href="css/parsley.css" media="screen">

    <?php include 'views/head_links.php'; ?>

</head>

<body>

    <!--==========================
    Header
    ============================-->
    <?php include 'views/nav.php'; ?>
    <!-- #header -->


    <!--==========================
    Intro Section
    ============================-->

    <div class="banner-home">
        <div class="content-camp">
            <h2>
                Stop the spread. Practise hand-washing and physical <br>
                distancing. Fighting COVID is Everybody's responsibility<br><br>


            </h2>
        </div>



    </div>
    <div class="cover-line">
        <img src="images/line.png" alt="">
    </div>




    <!-- #intro -->

    <!-- ===================================== PRODUCTS ===================================== -->

    <div class="product product-new">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <h1 style="padding-right: 20px">WHAT IS CORONA VIRUS</h1>
                    <div class="under-line img9">
                        <img src="images/line.png" style="margin-left: 20px" alt="">
                    </div>
                    <P>
                        Its a group of viruses that mostly affect the respiratory system. There is a new
                        strain of the virus called 2019 Novel Corona virus (ncov2019) that has not been
                        previously identified in human
                    </P>
                    <img src="img/covid/corona.png" style="width: 150px;" alt="">
                </div>
                <div class="col-md-6">
                    <h1 style="padding-right: 20px">QUICK FACTS</h1>
                    <div class="under-line img6">
                        <img src="images/line.png" style="margin-left: 20px" alt="">
                    </div>
                    <ol class="text-left order-position">
                        <li>Anyone can get the virus.</li>
                        <li>The virus is zoonotic meaning that it can be transmitted between animals and people.</li>
                        <li>It spreads from person to person through respiratory droplets when someone sneezes or coughs.</li>
                        <li>It's possible to get the virus by touching an object or surface that has the virusand then touching
                            your mouth, nose or possibly your eyes. It can survive upto 48hrs outside the body.</li>
                    </ol>
                </div>
            </div>
            <br>

        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 corona-sign">
                <div class="col-6">
                    <img class="cvd-img" src="img/covid/water.png" alt="">
                </div>
                <div class="col-6">
                    <p>Wash your hands several times a day with soap and water</p>
                </div>
            </div>

            <div class="col-md-6 corona-sign">
                <div class="col-6">
                    <img class="cvd-img" src="img/covid/sanitize.png" alt="">
                </div>
                <div class="col-6">
                    <p>Use the hand sanitizers</p>
                </div>
            </div>

            <div class="col-md-6 corona-sign">
                <div class="col-6">
                    <img class="cvd-img" src="img/covid/sneezing.png" alt="">
                </div>
                <div class="col-6">
                    <p>Cough and sneeze into a tissue or your elbow Avoid</p>
                </div>
            </div>

            <div class="col-md-6 corona-sign">
                <div class="col-6">
                    <img class="cvd-img" src="img/covid/hand.png" alt="">
                </div>
                <div class="col-6">
                    <p>Avoid greeting others by shaking hands, hugging or kissing</p>
                </div>
            </div>

            <div class="col-md-6 corona-sign">
                <div class="col-6">
                    <img class="cvd-img" src="img/covid/people.png" alt="">
                </div>
                <div class="col-6">
                    <p>Keep your distance from other people when talking or when
                        queuing</p>
                </div>
            </div>

            <div class="col-md-6 corona-sign">
                <div class="col-6">
                    <img class="cvd-img" src="img/covid/child.png" alt="">
                </div>
                <div class="col-6">
                    <p style="width: 120%">If you are unwell or feeling sick with the following symptoms-dry
                        cough, fever, shortness of breath and general body weakness,
                        contact a medical professional, do not come to work</p>
                </div>
            </div>

            <div class="col-md-6 corona-sign">
                <div class="col-6">
                    <img class="cvd-img" src="img/covid/mask.png" alt="">
                </div>
                <div class="col-6">
                    <p>Always wear a mask while out in public places</p>
                </div>
            </div>

            <div class="offset-6">

            </div>
        </div>
    </div>


    <!-- =====================================ACESS SERVICE===================================== -->
    <div class="container-fluid12">

    </div>
    </div>
    <!-- =====================================FOOTER===================================== -->
    <?php include 'views/footer.php'; ?>
    <!-- #footer -->


    <?php include 'views/get_cover.php'; ?>


    <!-- loading scripts -->
    <?php
    require_once 'inc/scripts.php';
    ?>
    <script src="js/parsley.min.js"></script>
    <script src="js/lead.js"></script>
</body>

</html>