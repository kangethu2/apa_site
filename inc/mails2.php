<?php
// claim report email
function claim_report(
    $subject,
    $businessEmail,
    $businessFullName,
    $clientEmail,
    $clientFullName,
    $body
) {

    //mailing claim report
    require_once '../mailer/PHPMailer.php';
    require_once '../mailer/SMTP.php';
    $mail = new PHPMailer(true);
    try {
        $mail->IsSMTP();
        $mail->isHTML(true);
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'echo';
        $mail->Host = 'mail.apainsurance.org';
        $mail->SMTPAuth   = true;
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;
        $mail->Username = 'apa.website@apollo.co.ke';
        $mail->Password = 'Apa321$321';

        $mail->setFrom('apa.website@apollo.co.ke', 'APA WEBSITE');
        $mail->addAddress($businessEmail, $businessFullName);
        $mail->addBCC('anthonybaru@gmail.com');
        $mail->addBCC('peter.chege@apollo.co.ke');
        $mail->addBCC('irene.akubania@apainsurance.org');
        $mail->addBCC('juddy.tonui@apollo.co.ke');
        $mail->AddReplyTo($clientEmail, $clientFullName);
        $mail->Subject = $subject;
        $mail->Body = $body;
        $send = $mail->send();
        if ($send) {
            $mail->ClearAddresses();
            //client response
            $subjectClient = 'CLAIM REPORT ACKNOWLEDGEMENT';
            $bodyClient = 'Dear ' . $clientFullName . ',<br><br> 
                We acknowledge receipt of your claim notification. 
                Our claims officer will contact you shortly to guide you and advise you accordingly. 
                Please do feel free to contact us via our contact centre number 0709912777 for more queries or clarification.';
            $mail->addAddress($clientEmail, $clientFullName);
            $mail->AddReplyTo('no-reply@apollo.co.ke', 'APA INSURANCE');
            $mail->Body = $bodyClient;
            $mail->Subject = $subjectClient;
            $mail->send();
            return 1;
        }
    } catch (Exception $e) {
        return strip_tags($e->errorMessage()); //Pretty error messages from PHPMailer
    } catch (\Exception $e) { //The leading slash means the Global PHP Exception class will be caught
        return $e->getMessage(); //Boring error messages from anything else!
    }
}

// claim life email
function claim_upload_email(
    $subject,
    $businessEmail,
    $businessFullName,
    $clientEmail,
    $clientFullName,
    $body,
    $documents
) {
    require_once '../mailer/PHPMailer.php';
    require_once '../mailer/SMTP.php';
    $mail = new PHPMailer(true);
    try {
        $mail->IsSMTP();
        $mail->isHTML(true);
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'echo';
        $mail->Host = 'mail.apainsurance.org';
        $mail->SMTPAuth   = true;
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;
        $mail->Username = 'apa.website@apollo.co.ke';
        $mail->Password = 'Apa321$321';

        $mail->setFrom('apa.website@apollo.co.ke', 'APA WEBSITE');
        $mail->addAddress($businessEmail, $businessFullName);
        $mail->addBCC('anthonybaru@gmail.com');
        $mail->addBCC('peter.chege@apollo.co.ke');
        $mail->addBCC('irene.akubania@apainsurance.org');
        $mail->addBCC('juddy.tonui@apollo.co.ke');
        $mail->AddReplyTo($clientEmail, $clientFullName);
        $mail->Subject = $subject;
        $mail->Body = $body;

        //looping through the available documents
        foreach ($documents as $key => $document) {
            $mail->addAttachment($document, $key);
        }
        $send = $mail->send();
        if ($send) {
            $mail->ClearAddresses();
            $mail->clearAttachments();
            //client response
            $subjectClient = 'CLAIM DOCUMENT UPLOAD ACKNOWLEDGEMENT';
            $bodyClient = 'Dear ' . $clientFullName . ',<br><br> 
                We acknowledge receipt of your claim documents. 
                Our claims officer will contact you shortly to guide you and advise you accordingly. 
                Please do feel free to contact us via our contact centre number 0709912777 for more queries or clarification.';
            $mail->addAddress($clientEmail, $clientFullName);
            $mail->AddReplyTo('no-reply@apollo.co.ke', 'APA INSURANCE');
            $mail->Body = $bodyClient;
            $mail->Subject = $subjectClient;
            $mail->send();
            return 1;
        }
    } catch (Exception $e) {
        return strip_tags($e->errorMessage()); //Pretty error messages from PHPMailer
    } catch (\Exception $e) { //The leading slash means the Global PHP Exception class will be caught
        return $e->getMessage(); //Boring error messages from anything else!
    }
}

// claim report email
function leads($clientFullName, $clientEmail)
{
    $subject = 'INFORMATION RECEIVED';
    $body = 'Dear ' . $clientFullName . ',<br><br> 
                Thank you for showing interest in our insurance solutions.  
                As a leading financial services group, we offer an array of affordable tailor made insurance and investment solutions. 
                Our customer experience executive will contact you shortly to expound on the cover selected.';
    //mailing claim report
    require_once '../mailer/PHPMailer.php';
    require_once '../mailer/SMTP.php';
    $mail = new PHPMailer(true);
    try {
        $mail->IsSMTP();
        $mail->isHTML(true);
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'echo';
        $mail->Host = 'mail.apainsurance.org';
        $mail->SMTPAuth   = true;
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;
        $mail->Username = 'apa.website@apollo.co.ke';
        $mail->Password = 'Apa321$321';

        $mail->setFrom('apa.website@apollo.co.ke', 'APA WEBSITE');
        $mail->addAddress($clientEmail, $clientFullName);
        $mail->addBCC('anthonybaru@gmail.com');
        $mail->AddReplyTo('no-reply@apollo.co.ke', 'APA INSURANCE');
        $mail->Subject = $subject;
        $mail->Body = $body;
        if ($mail->send()) {
            return 1;
        }
    } catch (Exception $e) {
        return strip_tags($e->errorMessage()); //Pretty error messages from PHPMailer
    } catch (\Exception $e) { //The leading slash means the Global PHP Exception class will be caught
        return $e->getMessage(); //Boring error messages from anything else!
    }
}

// claim report email
function apollo_book(
    $room_name,
    $start_date,
    $end_date,
    $start_time,
    $end_time,
    $company_name,
    $phone,
    $email,
    $capacity,
    $more_information,
    $subject,
    $body
) {

    //apollo centre booking
    require_once '../mailer/PHPMailer.php';
    require_once '../mailer/SMTP.php';
    $mail = new PHPMailer(true);
    try {
        $mail->IsSMTP();
        $mail->isHTML(true);
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'echo';
        $mail->Host = 'mail.apainsurance.org';
        $mail->SMTPAuth   = true;
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;
        $mail->Username = 'apa.website@apollo.co.ke';
        $mail->Password = 'Apa321$321';

        $mail->setFrom('apa.website@apollo.co.ke', 'APA WEBSITE');
        $mail->addAddress('info@apollocentre.org', 'APOLLO CENTRE');
        $mail->addBCC('anthonybaru@gmail.com');
        $mail->AddReplyTo($email, $company_name);
        $mail->Subject = $subject;
        $mail->Body = $body;
        $send=$mail->send();
        if ($send) {
            $mail->clearAddresses();
            //client response
            $subjectClient = 'Booking Acknowledgement';
            $bodyClient = 'Dear ' . $company_name . ',<br> 
                We acknowledge receipt of your booking. 
                Our officials will contact you shortly to guide and advise you accordingly. 
                Please do feel free to contact us via our contact centre number 0709912777 for more queries or clarification.';
            $mail->addAddress($email, $company_name);
            $mail->addReplyTo('no-reply@apollo.co.ke', 'APA INSURANCE');
            $mail->Body = $bodyClient;
            $mail->Subject = $subjectClient;
            $mail->send();
            return 1;
        }
    } catch (Exception $e) {
        return strip_tags($e->errorMessage()); //Pretty error messages from PHPMailer
    } catch (\Exception $e) { //The leading slash means the Global PHP Exception class will be caught
        return $e->getMessage(); //Boring error messages from anything else!
    }
}


//campaign auto response email
function campaign($clientFullName, $clientEmail, $product)
{
    switch ($product) {
        case '45':
            $subject = 'APOLLO BALANCED FUND';
            $body = 'Dear ' . $clientFullName . ',<br><br> 
                Thank you for showing interest in our product. <br>';
            $body.='The Apollo Balanced Fund invests in a well balanced portfolio comprising of shares of growth oriented companies and secure high interest bearing investments. ';
            $body.='The diversification boosts return by providing a cushion during a down turn in one asset class. ';
            $body.='It is ideal for investors looking for medium to long-term growth of their capital with a 3-5-year investment horizon.<br><br>';
            $body.='<b>Benefits and Advantages</b><br>';
            $body.='<ol>';
            $body.='<li><b>Amount: </b>Minimum initial investment amount KES 10,000</li>';
            $body.='<li><b>Top ups: </b>Investor can top up anytime as they wish with as little as KES 5,000</li>';
            $body.='<li><b>Efficiency: </b>Quick and easy access to funds</li>';
            $body.='<li><b>Flexibility: </b>The period to invest is not fixed or contractual</li>';
            $body.='<li><b>Accessibility: </b>Funds are availed within 2-4 working days of issuing a withdrawal request</li>';
            $body.='<li><b>Performance: </b>Performance will be subject to volatility of the market and individual company performance</li>';
            $body.='</ol><br><br>';
            $body.='<b>FEATURES</b><br>';
            $body.='<ol>';
            $body.='<li>The Fund aims to achieve capital appreciation through investments in listed securities of companies on the Nairobi Stock Exchange and secure interest bearing investments</li>';
            $body.='<li>Investors in this Fund should have medium to long term investment horizon</li>';
            $body.='<li>The Fund is suitable for investors who are seeking long term capital growth</li>';
            $body.='<li>Income is distributed quarterly</li>';
            $body.='</ol>';
               
            break;
        
        case '46':
            $subject = 'APOLLO EQUITY FUND';
            $body = 'Dear ' . $clientFullName . ',<br><br> 
                Thank you for showing interest in our product. <br>';
            $body.='The Apollo Equity Fund invests in professionally selected securities listed on the Nairobi Securities Exchange. ';
            $body.='It is a suitable investment option for investors looking to put money in the stock market but lack the professional capacity to select and manage their portfolio. ';
            $body.='The fund is ideal for investors with a long-term investment horizon.<br><br>';
            $body.='<b>Benefits and Advantages</b><br>';
            $body.='<ol>';
            $body.='<li><b>Amount: </b>Minimum initial investment amount KES 10,000</li>';
            $body.='<li><b>Top ups: </b>Investor can top up anytime as they wish with as little as KES 5,000</li>';
            $body.='<li><b>Efficiency: </b>Quick and easy access to funds</li>';
            $body.='<li><b>Flexibility: </b>The period to invest is not fixed or contractual</li>';
            $body.='<li><b>Accessibility: </b>Funds are availed within 2-4 working days of issuing a withdrawal request</li>';
            $body.='<li><b>Performance: </b>Performance will be subject to volatility of the market and individual company performance</li>';
            $body.='</ol><br><br>';
            $body.='<b>FEATURES</b><br>';
            $body.='<ol>';
            $body.='<li>The fund aims to achieve capital appreciation through investments in listed securities of companies on the Nairobi Stock Exchange</li>';
            $body.='<li>Investors in this Fund should have a medium to long term investment horizon</li>';
            $body.='<li>The fund is suitable for investors who are seeking long term capital growth</li>';
            $body.='<li>The fund targets a medium to high risk profile by Investing in stocks of several sectors of the economy</li>';
            $body.='<li>Income is distributed semi-annually</li>';
            $body.='</ol>';

            break;
        
        case '47':
            $subject = 'APOLLO MONEY MARKET FUND';
            $body = 'Dear ' . $clientFullName . ',<br><br> 
                Thank you for showing interest in our product. <br>';
            $body.='Apollo Money Market fund is a conservative fund that provides the investor with capital preservation while offering high levels of income. ';
            $body.='The funds are invested in quality short-term securities (Interest Bearing Assets, Fixed Deposits, treasury bills and bonds etc.) thus ensuring maximum safety to the investor.<br><br>';
            $body.='<b>Benefits and Advantages</b><br>';
            $body.='<ol>';
            $body.='<li><b>Amount: </b>Minimum initial investment amount KES 10,000</li>';
            $body.='<li><b>Top ups: </b>Investor can top up anytime as they wish with as little as KES 2,000</li>';
            $body.='<li><b>Efficiency: </b>Quick and easy access to funds</li>';
            $body.='<li><b>Interest: </b>Interest is compounded and is calculated on a daily basis</li>';
            $body.='<li><b>Flexibility: </b>The period to invest is not fixed or contractual</li>';
            $body.='<li><b>Accessibility: </b>Funds are availed within 2-4 working days of issuing a withdrawal request</li>';
            $body.='<li><b>Performance: </b>Historically the money market has performed better than the banks and treasury bills</li>';
            $body.='<li><b>Rate: </b>The rate is not guaranteed as the value of fixed income securities will change due to changes in interest rates in the market</li>';
            $body.='</ol><br><br>';
            $body.='<b>FEATURES</b><br>';
            $body.='<ol>';
            $body.='<li>Funds are invested in high quality Interest Bearing Assets, Fixed Deposits and near cash holdings in the Kenyan Market</li>';
            $body.='<li>Fund consists of compounded interest that is calculated daily and distributed monthly into the clients account</li>';
            $body.='<li>It is free to set up the account, no initial fees</li>';
            $body.='<li>It is a low risk fund</li>';
            $body.='<li>Fund is ideal as a safe parking for money in the short-term. It is a good option for individual, corporate and joint investors</li>';
            $body.='</ol>';

            break;
        
        default:
            
            break;
    }
    
    //mailing claim report
    require_once '../mailer/PHPMailer.php';
    require_once '../mailer/SMTP.php';
    $mail = new PHPMailer(true);
    try {
        $mail->IsSMTP();
        $mail->isHTML(true);
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'echo';
        $mail->Host = 'mail.apainsurance.org';
        $mail->SMTPAuth   = true;
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;
        $mail->Username = 'apa.website@apollo.co.ke';
        $mail->Password = 'Apa321$321';

        $mail->setFrom('apa.website@apollo.co.ke', 'APA WEBSITE');
        $mail->addAddress($clientEmail, $clientFullName);
        $mail->addBCC('anthonybaru@gmail.com');
        $mail->addBCC('peter.chege@apollo.co.ke');
        $mail->addReplyTo('no-reply@apollo.co.ke', 'APA INSURANCE');
        $mail->Subject = $subject;
        $mail->Body = $body;
        if ($mail->send()) {
            return 1;
        }
    } catch (Exception $e) {
        return strip_tags($e->errorMessage()); //Pretty error messages from PHPMailer
    } catch (\Exception $e) { //The leading slash means the Global PHP Exception class will be caught
        return $e->getMessage(); //Boring error messages from anything else!
    }
}
