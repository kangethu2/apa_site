<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Apollo Group Heads - APA Insurance </title>
    <link rel="stylesheet" href="css/companies.css" media="screen">

    <?php include 'views/head_links.php'; ?>

</head>

<body>

    <!--==========================
    Header
    ============================-->
    <?php include 'views/nav.php'; ?>
    <!-- #header -->

    <!--==========================
    Intro Section
    ============================-->
    <div class="banner-group">

    </div>
    <div class="cover-line">
        <img src="images/line.png" alt="">
    </div>


    <!-- #intro -->

    <!-- ===================================== CHILD MENU BAR ===================================== -->

    <div class="company-nav">
        <ul class="nav justify-content-center">
            <li class="nav-item">
                <a class="nav-link" href="companies.php">COMPANIES</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="leadership.php">LEADERSHIP</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link dropdown-toggle" href="management_group.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">MANAGMENT</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">

                </div>


            </li>
            <li class="nav-item">
                <a class="nav-link" href="financial_report.php">FINANCIAL REPORTS</a>
            </li>
        </ul>
    </div>


    <!-- SUB NAV -->

    <div class="sub-nav">
        <ul class="nav justify-content-center">
            <li class="nav-item">
                <a class="nav-link" href="management_group.php">GROUP HEADS </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="management.php">APA INSURANCE </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="management_life.php">APA LIFE </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="management_asset.php">APOLLO ASSET MANAGEMENT</a>
            </li>

        </ul>
    </div>


    <!-- ===================================== ABOUT US ===================================== -->


    <div class="container-fluid12">
        <div class="container apollo">
            <h1 class="wow fadeInUp" data-wow-delay="0.1s">APOLLO GROUP HEADS </h1>
            <div class="under-line img6">
                <img src="images/line.png" alt="">
            </div>

            <p class="container content-offer wow fadeInUp" data-wow-delay="0.1s">
                APA has become the largest insurance firms in Kenya and the most innovative in product offerings.
                APA Insurance underwrites General Insurance risks such as Motor, Agriculture, Marine, and Micro
                Insurance.
                We also underwrite Individual and Family Health Insurance.
            </p>
        </div>

        <!-- APA INSURANCE -->
        <div class="company-box apollo">
            <div class="container">

                <div class="row lead-img">

                    <div class="col-md-3 leader-img wow fadeInUp" data-wow-delay="0.1s">
                        <img src="img/apa4/keval.jpg" class="img" alt="">
                        <h5 class="title-name">
                            Keval Shah<br>
                            <small>Group Chief Finance Officer </small>
                        </h5>

                        <p>
                            Keval has over 25 year’s experience across accounting, tax, advisory...
                        </p>
                        <button class="btn btn-primary" data-target="#modal-full101" uk-toggle>Read more</button>
                    </div>

                    <div class="col-md-3 leader-img wow fadeInUp" data-wow-delay="0.1s">
                        <img src="img/apa3/beth.jpg" class="img" alt="">
                        <h5 class="title-name">
                            Beth Kajuju <br>
                            <small>Group Head of Human Resource</small>
                        </h5>

                        <p>
                            Beth Kajuju is a seasoned HR Professional with over 15 years’ experience of...
                        </p>
                        <button class="btn btn-primary" data-target="#modal-full10" uk-toggle>Read more</button>
                    </div>

                    <div class="col-md-3 leader-img wow fadeInUp" data-wow-delay="0.1s">
                        <img src="img/apa4/james.jpg" class="img" alt="">
                        <h5 class="title-name">
                            James Nyakomitta<br>
                            <small>Group Chief Information Officer</small>
                        </h5>

                        <p>
                            James is an experienced Information & Communication Tech...
                        </p>
                        <button class="btn btn-primary" data-target="#modal-full11" uk-toggle>Read more</button>
                    </div>

                    <!-- <div class="col-md-3 leader-img wow fadeInUp" data-wow-delay="0.1s">
                        <img src="img/apa4/chris.jpg" class="img" alt="">
                        <h5 class="title-name">
                            Chris Ngala<br>
                            <small>Group Head of Internal Audit</small>
                        </h5>

                        <p>
                            Chris is the Group Head of Internal Audit with over 20 combined years in the...
                        </p>
                        <button class="btn btn-primary" data-target="#modal-full12" uk-toggle>Read more</button>
                    </div> -->

                    <div class="col-md-3 leader-img wow fadeInUp" data-wow-delay="0.1s">
                        <img src="img/apa4/benjamin.jpg" class="img" alt="">
                        <h5 class="title-name">
                            Benjamin Otieno<br>
                            <small>Group Risk Manager</small>
                        </h5>

                        <p>
                            Benjamin has an experience spanning over 15 years in banking, development...
                        </p>
                        <button class="btn btn-primary" data-target="#modal-full13" uk-toggle>Read more</button>
                    </div>

                    <div class="col-md-3 leader-img wow fadeInUp" data-wow-delay="0.1s">
                        <img src="img/apa4/judy.jpg" class="img" alt="">
                        <h5 class="title-name">
                            Judith Bogonko Juma<br>
                            <small>Head of Customer Experience & Innovation</small>
                        </h5>

                        <p>
                        Judith Bogonko Juma is the Head of Customer Experience and Innovation...
                        </p>
                        <button class="btn btn-primary" data-target="#modal-full14" uk-toggle>Read more</button>
                    </div>

                    <div class="col-md-3 leader-img wow fadeInUp" data-wow-delay="0.1s">
                        <img src=" img/managment/grace.jpg" class="img" alt="">
                        <h5 class="title-name">
                            Grace Nganga<br>
                            <small>Head of Corporate Communications</small>
                        </h5>

                        <p>
                            Grace Nganga is a seasoned professional in communication and...
                        </p>
                        <button class="btn btn-primary" data-target="#modal-full15" uk-toggle>Read more</button>
                    </div>

                    <div class="offset-6">

                    </div>

                </div>

                <div class="lead-img">
                   

                   

                </div>

            </div>
        </div>


    </div>




    <!-- =====================================  Keval Shah ===================================== -->

    <div id="modal-full101" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
            <div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle" uk-grid>
                <div class="uk-background-cover" style="background-image: url('img/managment/keval.jpg');" uk-height-viewport></div>
                <div class="uk-padding-large">
                    <h1>
                        Keval Shah
                    </h1>
                    <h2 class="small-head">
                        Group Chief Finance Officer
                    </h2>
                    <p class="text-justify">
                        Keval has over 25 year’s experience across accounting, tax, advisory and financial management
                        disciplines in
                        both manufacturing and financial services industries. He is a graduate of Loughborough
                        University, United Kingdom,
                        Fellow of the Association of Chartered Certified Accountants (FCCA) and a Member of the
                        Chartered Institute of
                        Management Accountants (ACMA).
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- =====================================  BETH KAJUJU  ===================================== -->

    <div id="modal-full10" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
            <div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle" uk-grid>
                <div class="uk-background-cover" style="background-image: url('img/apa3/beth.jpg');" uk-height-viewport></div>
                <div class="uk-padding-large">
                    <h1>
                        Beth Kajuju
                    </h1>
                    <h2 class="small-head">
                        Group Head of Human Resource
                    </h2>
                    <p class="text-justify">
                    Beth Kajuju is a seasoned HR Professional with over 15 years’ experience of both strategic and innovative HR processes
                    that support the translation of business vision into actionable, value-adding goals through business partnering. She 
                    has extensive HR exposure having worked in various industries including publishing, pharmaceutical, manufacturing and 
                    security services. She holds a Master’s degree in HR Management, a B.COM (Accounting & Bus. Administration) and is a 
                    certified member of IHRM.

                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- =====================================  James Nyakomitta ===================================== -->

    <div id="modal-full11" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
            <div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle" uk-grid>
                <div class="uk-background-cover" style="background-image: url('img/managment/james.jpg');" uk-height-viewport></div>
                <div class="uk-padding-large">
                    <h1>
                        James Nyakomitta
                    </h1>
                    <h2 class="small-head">
                        Group Chief Information Officer
                    </h2>
                    <p class="text-justify">
                        James is an experienced Information & Communication Technology professional with extensive
                        background
                        in planning, organising and implementing Information Communication and related Technologies that
                        support
                        business operations. He holds a Masters degree in Business Administration and is a graduate of
                        Computer
                        Science. James has served in various ICT leadership roles for the past 14 years in both
                        Insurance and
                        Banking sectors, with continued success in meeting business/operational goals. He is a member
                        of
                        Information Systems Audit and Control Association (ISACA).

                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- ===================================== Chris Ngala ===================================== -->

    <!-- <div id="modal-full12" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
            <div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle" uk-grid>
                <div class="uk-background-cover" style="background-image: url('img/managment/chris.jpg');" uk-height-viewport></div>
                <div class="uk-padding-large">
                    <h1>
                        Chris Ngala
                    </h1>
                    <h2 class="small-head">
                        Group Head of Internal Audit
                    </h2>
                    <p class="text-justify">
                        Chris is the Group Head of Internal Audit with over 20 combined years in the Accounting and
                        Audit professions.
                        He is a member of the Institute of Certified Public Accountants of Kenya and the Information
                        Systems and Control
                        Association. Prior to joining the Apollo Group, he held similar positions at Jubilee
                        Holdings and NIC Bank.
                        He also worked in various capacities at Commercial Bank of Africa Limited. He is a member of the
                        Institute of
                        Internal Auditors.

                    </p>
                </div>
            </div>
        </div>
    </div> -->

    <!-- ===================================== Benjamin Otieno ===================================== -->

    <div id="modal-full13" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
            <div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle" uk-grid>
                <div class="uk-background-cover" style="background-image: url('img/managment/benjamin.jpg');" uk-height-viewport></div>
                <div class="uk-padding-large">
                    <h1>
                        Benjamin Otieno
                    </h1>
                    <h2 class="small-head">
                        Group Risk Manager
                    </h2>
                    <p class="text-justify">
                        Benjamin has an experience spanning over 15 years in banking, development financing and
                        insurance industries
                        having worked on various risk management and strategy assignments. He holds a bachelor’s degree
                        in Mathematics
                        and Computer Science and a MSc in Information Systems. He is a member of the Global Association
                        of Risk Professionals
                        (GARP) and Information Systems Audit and Control Association (ISACA).
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- ===================================== Judith Bogonko Juma ===================================== -->

    <div id="modal-full14" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
            <div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle" uk-grid>
                <div class="uk-background-cover" style="background-image: url('img/managment/judy.jpg');" uk-height-viewport></div>
                <div class="uk-padding-large">
                    <h1>
                        Judith Bogonko Juma
                    </h1>
                    <h2 class="small-head">
                        Head of Customer Experience & Innovation
                    </h2>
                    <p class="text-justify">
                    Judith Bogonko Juma is the Head of Customer Experience and Innovation, and has 10 years of experience in the insurance industry. She holds a Bachelor’s degree in Actuarial Science from JKUAT, a postgraduate degree in Project Management from KIM an MBA from Daystar University and a Diploma in Insurance from ACII.</p>
                </div>
            </div>
        </div>
    </div>

    <!-- ===================================== Grace Nganga ===================================== -->

    <div id="modal-full15" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
            <div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle" uk-grid>
                <div class="uk-background-cover" style="background-image: url('img/managment/grace_full.jpg');" uk-height-viewport></div>
                <div class="uk-padding-large">
                    <h1> 
                        Grace Nganga
                    </h1>
                    <h2 class="small-head">
                        Head of Corporate Communications
                    </h2>
                    <p class="text-justify">
                        She is a seasoned professional in communication and marketing with over 10 years’ experience working
                        for both local and multinational organizations. She holds a Bachelor’s Degree in Management from
                        Southern Adventist University, a Master’s Degree in Marketing from University of Nairobi and a
                        Postgraduate Diploma in Marketing from Kenya Institute of Management. She is a member of the
                        Marketing Society of Kenya.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- ===================================== Charles Wambua ===================================== -->

    <div id="modal-full16" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
            <div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle" uk-grid>
                <div class="uk-background-cover" style="background-image: url('img/managment/charlse.jpg');" uk-height-viewport></div>
                <div class="uk-padding-large">
                    <h1>
                        Charles Wambua
                    </h1>
                    <h2 class="small-head">
                        Head of Microinsurance & Agribusiness
                    </h2>
                    <p class="text-justify">
                        Charles holds a Bachelors degree in Education, a Masters degree in Business Administration and
                        is currently pursuing a
                        Masters degree in Economics. He has over 22 years of experience in economic development programs
                        both locally and across
                        several countries in Africa and has played key roles in the microinsurance landscape surveys for
                        Kenya and for Africa
                        jointly published by Munique Re Foundation and GIZ program “Making Finance Work for Africa”.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- =====================================  Janette Awidhi ===================================== -->

    <div id="modal-full17" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
            <div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle" uk-grid>
                <div class="uk-background-cover" style="background-image: url('img/managment/janette.jpg');" uk-height-viewport></div>
                <div class="uk-padding-large">
                    <h1>
                        Janette Awidhi
                    </h1>
                    <h2 class="small-head">
                        Head of Legal and Claims
                    </h2>
                    <p class="text-justify">
                        Janette has over 25 years of proven track record in claims management. She holds a Bachelor of
                        Science in
                        Insurance from the University of Nairobi and a Diploma ACII from the Insurance Institute, UK.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- =====================================FOOTER===================================== -->
    <?php include 'views/footer.php'; ?>
    <!-- #footer -->
    <?php
    require_once 'inc/scripts.php';
    ?>

</body>

</html>