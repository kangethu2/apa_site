<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>INDIVIDUAL PENSION PLAN</title>

    <!-- META DATA DESCRIPTION FOR GOOGLE SEARCH RESULT -->
    <meta name="description" content="We’ll help you think differently about retirement – more 
    optimistic about tomorrow and confident about taking control. You may see yourself traveling, 
    volunteering or simply spoiling your grand kids">
    <meta name="keywords" content="insurance cover, apa insurance kenya, apa insurance limited, 
    pension plan , pension cover, individual pension cover, insurance online quotation, apa online services">
    <meta name="author" content="">

    <!-- FACEBOOK MEATADATA -->
    <meta property="og:url" content="https://www.apainsurance.org/campaign_individual_pension_plan.php" />
    <meta property="og:type" content="Homepage" />
    <meta property="og:title" content="APA Insurance, APA Insurance Kenya, APA Insurance Limited" />
    <meta property="og:description" content="We’ll help you think differently about retirement – more 
    optimistic about tomorrow and confident about taking control. You may see yourself traveling, 
    volunteering or simply spoiling your grand kids" />

    <!-- STYLESHEET -->
    <link rel="stylesheet" href="css/apollo_centre.css" media="screen">
    <link rel="stylesheet" href="css/modal.css" media="screen">
    <link rel="stylesheet" href="css/product.css" media="screen">
    <link rel="stylesheet" href="css/parsley.css" media="screen">

    <?php include 'views/head_links.php'; ?>

</head>

<body>

    <!--==========================
    Header
    ============================-->
    <?php include 'views/nav.php'; ?>
    <!-- #header -->
<br><br>
<div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-top" uk-grid>
            <div class="uk-background-cover uk-visible@m" style="background-image: url('img/campaign/ipp.jpg'); background-size: contain;" uk-height-viewport>
            </div>
            <div class="uk-padding-large">
            <br>
            <div class="form-container uk-hidden@m">
                <h3> INDIVIDUAL PENSION PLAN</h3>
                <p>Get an Individual pension plan that gives you competitive returns, allows for flexible contributions and ease of transfer. </p>
                <hr>
            </div>
                <form id="form" action="<?= $_SERVER['PHP_SELF']; ?>" method="POST" class="form-container">
                    <h3 for="inputAddress" class="comp-detail">PERSONAL DETAILS</h3>
                    <div class="container">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="full_name">Full Name</label>
                                <input name="full_name" type="text" class="form-control" id="full_name" placeholder="John Doe" value="" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="phone">Mobile Number</label>
                                <input name="phone" type="tel" class="form-control" id="phone" data-parsley-pattern="^(?:254|\+254|0)?(7(?:(?:[123456789][0-9])|(?:0[0-8])|(4[0-1]))[0-9]{6})$" data-parsley-trigger="keyup" placeholder="07xx 254 xxx" value="" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="email">Email Address</label>
                                <input name="email" type="email" class="form-control" id="email" placeholder="john@gmail.com" value="" required data-parsley-type="email" data-parsley-trigger="keyup">
                            </div>
                            <div class=" form-group col-md-6">
                                <label for="location">Location</label>
                                <input name="location" type="text" class="form-control" id="location" placeholder="Nairobi" value="" required>
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="form-group col-md-12">
                        <h3 for="inputAddress">COVER DETAILS</h3>
                    </div>

                    <div class="container">

                        <div class="row">

                            <div class="form-group col-md-12">
                                <label for="age">Your Age (yrs)</label>
                                <select id="age" name="age" class="form-control" required>
                                    <option value="-1" selected disabled>Select your age range...</option>
                                    <option value="18 - 23"> 18 - 23 </option>
                                    <option value="24 - 25"> 24 - 29 </option>
                                    <option value="30 - 34"> 30 - 34 </option>
                                    <option value="35 - 39"> 35 - 39 </option>
                                    <option value="40 - 44"> 40 - 44 </option>
                                    <option value="45 - 49"> 45 - 49 </option>
                                    <option value="50 - 54"> 50 - 54 </option>
                                    <option value="55 - 59"> 55 - 59 </option>
                                    <option value="60 - 64"> 60 - 64 </option>
                                    <option value="65 - 69"> 65 - 69 </option>
                                    <option value="Above 70"> Above 70 </option>
                                </select>
                            </div>
                        </div>


                        <div style="display: none">
                            <input type="hidden" id="product_id" name="product_id" value="24">
                            <input type="hidden" id="product_category_id" name="product_category_id" value="14">
                        </div>
                        <div class="row book-btn">
                            <div class="col-md-12">
                                <button type="submit" name="request" class="btn btn-primary">SUBMIT <i class="fas fa-paper-plane"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>


            </div>
        </div>

    <!-- =====================================FOOTER===================================== -->
    <?php include 'views/footer.php'; ?>
    <!-- #footer -->


    <?php include 'views/get_cover.php'; ?>


    <!-- loading scripts -->
    <?php
    require_once 'inc/scripts.php';
    ?>
    <script src="js/parsley.min.js"></script>
    <script src="js/lead.js"></script>
</body>

</html>