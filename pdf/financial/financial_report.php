<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Financial Reports - APA Insurance</title>
    <link rel="stylesheet" href="css/financial.css" media="screen">
    <link rel="stylesheet" href="css/parsley.css" media="screen">
    <link rel="stylesheet" href="css/parsley.css" media="screen">


    <?php include 'views/head_links.php'; ?>

</head>

<body>

    <!--==========================
    Header
    ============================-->
    <?php include 'views/nav.php'; ?>
    <!-- #header -->

    <!--==========================
    Intro Section
    ============================-->
    <!-- <div class="banner-about">

    </div> -->
    <div class="cover-line">
        <img src="images/line.png" alt="">
    </div>


    <!-- #intro -->
    <div class="container text-center header-finance">
        <h1>
            FINANCIAL REPORTS
        </h1>
        <div class="under-line img6">
            <img src="images/line.png" alt="">
        </div>
        <br>

        <section>
            <div class="container">
                <!-- APA INSURANCE FINANCIAL REPORTS -->
                <div class="row text-left wow fadeInUp" data-wow-delay="0.1s">

                    <div class="col-md-6 col-lg-3 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="85" height="50" src="img/doc/fn_2019.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APA INSURANCE</h3>
                                        <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial year for <b>2019</b></time></p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    <div class="c0l-6">
                                        <a href="https://online.fliphtml5.com/pocca/eiao/" class="uk-button uk-button-text my-text" target="_blank">Read more <i class="fas fa-eye"></i></a>
                                    </div>
                                    <div class="c0l-6">
                                        <a href="pdf/financial/ke/APAI Annual Report 2019.pdf" class="uk-button uk-button-text my-text" target="_blank">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="85" height="50" src="img/doc/fin_1.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APA INSURANCE</h3>
                                        <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial year for <b>2018</b></time></p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    <div class="c0l-6">
                                        <a href="digital/financial_report/apa_2018_new" class="uk-button uk-button-text my-text" target="_blank">Read more <i class="fas fa-eye"></i></a>
                                    </div>
                                    <div class="c0l-6">
                                        <a href="pdf/financial/ke/2018 APA Insurance Kenya Annual Report and Financial Statements.pdf" class="uk-button uk-button-text my-text" target="_blank">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="85" height="50" src="img/doc/apa_2017.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APA INSURANCE</h3>
                                        <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial year for <b>2017</b></time></p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    <div class="c0l-6">
                                        <a href="digital/financial_report/apa_2017" class="uk-button uk-button-text my-text" target="_blank">Read more <i class="fas fa-eye"></i></a>
                                    </div>
                                    <div class="c0l-6">
                                        <a href="pdf/financial/ke/2017 APA Insurance Kenya Annual Report and Financial Statements.pdf"
                                        class="uk-button uk-button-text my-text" target="_blank">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="85" height="50" src="img/doc/apa_2016.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APA INSURANCE</h3>
                                        <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial year for <b>2016</b></time></p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    <div class="c0l-6">
                                        <a href="digital/financial_report/apa_2016" class="uk-button uk-button-text my-text" target="_blank">Read more <i class="fas fa-eye"></i></a>
                                    </div>
                                    <div class="c0l-6">
                                        <a href="pdf/financial/ke/2016 APA Insurance Kenya Annual Report and Financial Statements.pdf" class="uk-button uk-button-text my-text">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                <!-- </div> -->

                <!-- APA LIFE FINANCIAL REPORT -->
                <!-- <div class="row text-left wow fadeInUp" data-wow-delay="0.2s"> -->

                    <div class="col-md-6 col-lg-3 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="100" height="50" src="img/doc/fnl_2019.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APA LIFE</h3>
                                        <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial year for <b>2018</b></time></p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    <div class="c0l-6">
                                        <a href="https://online.fliphtml5.com/pocca/wcpb" class="uk-button uk-button-text my-text" target="_blank">Read more <i class="fas fa-eye"></i></a>
                                    </div>
                                    <div class="c0l-6">
                                        <a href="pdf/financial/ke/APAL Annual Report 2019.pdf" class="uk-button uk-button-text my-text" target="_blank">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="100" height="50" src="img/doc/life_1.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APA LIFE</h3>
                                        <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial year for <b>2018</b></time></p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    <div class="c0l-6">
                                        <a href="digital/financial_report/life_2018_new" class="uk-button uk-button-text my-text" target="_blank">Read more <i class="fas fa-eye"></i></a>
                                    </div>
                                    <div class="c0l-6">
                                        <a href="pdf/financial/ke/2018 APA Life Kenya Annual Report and Financial Statements.pdf" class="uk-button uk-button-text my-text" target="_blank">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="100" height="50" src="img/doc/life_2017.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APA LIFE</h3>
                                        <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial year for <b>2017</b></time></p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    <div class="c0l-6">
                                        <a href="digital/financial_report/life_2017" class="uk-button uk-button-text my-text" target="_blank">Read more <i class="fas fa-eye"></i></a>
                                    </div>
                                    <div class="c0l-6">
                                        <a href="pdf/financial/ke/2017 APA Life Kenya Annual Report and Financial Statements.pdf" class="uk-button uk-button-text my-text" target="_blank">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="100" height="50" src="img/doc/life_2016.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APA LIFE</h3>
                                        <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial year for <b>2016</b></time></p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    <div class="c0l-6">
                                        <a href="digital/financial_report/life_2016" class="uk-button uk-button-text my-text" target="_blank">Read more <i class="fas fa-eye"></i></a>
                                    </div>
                                    <div class="c0l-6">
                                        <a href="pdf/financial/ke/2016 APA Life Kenya Annual Report and Financial Statements.pdf" class="uk-button uk-button-text my-text" target="_blank">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                <!-- </div> -->

                <!-- APOLLO ASSET MANAGEMENT FINANCIAL REPORT -->
                <!-- <div class="row text-left wow fadeInUp" data-wow-delay="0.2s"> -->

                    <div class="col-md-6 col-lg-3 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header"> 
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="100" height="50" src="img/doc/asset_2.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APOLLO BALANCED FUND </h3>
                                        <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial statement for <b>2019</b></time></p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    
                                    <div class="c0l-6">
                                        <a href="pdf/financial/ke/2018 Apollo Asset Management Unit Trust Abridged Financial Statements.pdf" class="uk-button uk-button-text my-text" target="_blank">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="100" height="50" src="img/doc/asset_2.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APOLLO EQUITY FUND </h3>
                                        <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial statement for <b>2019</b></time></p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    <div class="c0l-6">
                                        <a href="pdf/financial/ke/2017 Apollo Asset Management Unit Trust Abridged Financial Statements.pdf" class="uk-button uk-button-text my-text" target="_blank">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="100" height="50" src="img/doc/asset_2.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APOLLO MONEY MARKET FUND </h3>
                                        <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial statement for <b>2019</b></time></p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    <div class="c0l-6">
                                        <a href="pdf/financial/MMF 2019 Audited Financials Resubmitted.pdf" class="uk-button uk-button-text my-text" target="_blank">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="100" height="50" src="img/doc/asset_2.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APOLLO UNIT TRUST ABRIDGED FINANCIAL REPORT</h3>
                                        <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial statement for <b>2019</b></time></p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    <div class="c0l-6">
                                        <a href="pdf/financial/ke/Abridged Financial Report - Yr 2019.pdf" class="uk-button uk-button-text my-text" target="_blank">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-4 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header"> 
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="100" height="50" src="img/doc/mmf_1.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APOLLO ASSET UNIT TRUST</h3>
                                        <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial year for <b>2018</b></time></p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    <div class="c0l-6">
                                        <a href="digital/financial_report/mmf_2018" class="uk-button uk-button-text my-text" target="_blank">Read more <i class="fas fa-eye"></i></a>
                                    </div>
                                    <div class="c0l-6">
                                        <a href="pdf/financial/ke/2018 Apollo Asset Management Unit Trust Abridged Financial Statements.pdf" class="uk-button uk-button-text my-text" target="_blank">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-4 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="100" height="50" src="img/doc/asset_2017.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APOLLO ASSET UNIT TRUST</h3>
                                        <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial year for <b>2017</b></time></p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    <div class="c0l-6">
                                        <a href="digital/financial_report/mmf_2017" class="uk-button uk-button-text my-text" target="_blank">Read more <i class="fas fa-eye"></i></a>
                                    </div>
                                    <div class="c0l-6">
                                        <a href="pdf/financial/ke/2017 Apollo Asset Management Unit Trust Abridged Financial Statements.pdf" class="uk-button uk-button-text my-text" target="_blank">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-4 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="100" height="50" src="img/doc/asset_1.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APOLLO ASSET UNIT TRUST</h3>
                                        <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial year for <b>2016</b></time></p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    <div class="c0l-6">
                                        <a href="digital/financial_report/mmf_2016" class="uk-button uk-button-text my-text" target="_blank">Read more <i class="fas fa-eye"></i></a>
                                    </div>
                                    <div class="c0l-6">
                                        <a href="pdf/financial/ke/2016 Apollo Asset Management Unit Trust Abridged Financial Statements.pdf" class="uk-button uk-button-text my-text" target="_blank">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-4 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header"> 
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="100" height="50" src="img/doc/mmf_1.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APOLLO ASSET UNIT TRUST PROXY FORM</h3>
                                        <!-- <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial year for <b>2018</b></time></p> -->
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    <div class="c0l-6">
                                        <a href="pdf/financial/APA AGM_PROXY FORM[1].PDF" class="uk-button uk-button-text my-text" target="_blank">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-4 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="100" height="50" src="img/doc/asset_2017.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APOLLO ASSET UNIT TRUST LETTER OF NO OBJECTION</h3>
                                        <!-- <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial year for <b>2017</b></time></p> -->
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    <div class="c0l-6">
                                        <a href="pdf/financial/No Objection Letter Apollo Asset Management.pdf" class="uk-button uk-button-text my-text" target="_blank">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-4 card-fin">
                        <div class="uk-card uk-card-default ">
                            <div class="uk-card-header">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                    <div class="uk-width-auto">
                                        <img class="uk-border-circle1" width="100" height="50" src="img/doc/asset_1.jpg">
                                    </div>
                                    <div class="uk-width-expand">
                                        <h3 class="uk-card-title uk-margin-remove-bottom my-custom">APOLLO ASSET UNIT TRUST AGM NOTICE</h3>
                                        <!-- <p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">Financial year for <b>2016</b></time></p> -->
                                    </div>
                                </div>
                            </div>
                            <div class="uk-card-footer">
                                <div class="row">
                                    <div class="c0l-6">
                                        <a href="pdf/financial/APA AGM_Notice[1].pdf" class="uk-button uk-button-text my-text" target="_blank">Download <i class="fas fa-download"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>

            </div>
            <br>

        </section>

    </div>


    <!-- =====================================FOOTER===================================== -->
    <?php include 'views/footer.php'; ?>
    <!-- #footer -->




    <?php require_once('inc/scripts.php'); ?>
    <script src="js/parsley.min.js"></script>
   

</body>

</html>